package se.queue.servlet.user;

import java.io.IOException;
import java.net.URLDecoder;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class Login extends MainServlet{
	private static final long serialVersionUID = 1611243447180579265L;


	public void handleRequest() throws IOException {

		String[] mail = req.getParameterValues("mail");
		String[] pass = req.getParameterValues("password");

		checkParameter(mail, "mail");
		checkParameter(pass, "pass");

		if(!paramFailed){
			logger.warning("param ok");
			String lowerMail = URLDecoder.decode(mail[0], "UTF-8").toLowerCase().replace(" ", "");
			User u = User.loadWithMailAndPass(lowerMail, pass[0]);
			if(u != null){
				se.queue.model.Login l = new se.queue.model.Login(u);
				logger.warning("user loaded");
				try {
					res.setContentType("application/json");
					out.print(l.toJson());
				} catch (JSONException e) {
					set500Error(e.getMessage());
				}
			}
			else{
				logger.warning("user did not load, mail: " + lowerMail + ", pass: " + pass[0]);
				set403Error("Mail and pass is not matching");
			}
		}
		out.close();
	}
}
