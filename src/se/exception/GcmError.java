package se.exception;

public class GcmError extends Exception{
	private static final long serialVersionUID = 6167516696084106481L;

	public GcmError(String msg){
		super(msg);
	}
}
