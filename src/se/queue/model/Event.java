package se.queue.model;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import javax.mail.MessagingException;

import se.exception.AnswerNotPartOfEventException;
import se.exception.GcmError;
import se.queue.utility.ContactMethod;
import se.queue.utility.TimeHelper;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class Event implements Comparable<Event>{
	public static final String DATASTORE_NAME = "Event";
	private long 	id,
	groupId,
	ruleId,
	time;
	private List<Long> answers;
	private boolean reminderSent = false,	//check for reminderService
			listLocked = false,		//check to see if its possible to answer to this event
			freeSpotOnLockReminderSent = false;

	//these are not in memory all the time
	private EventRule er;
	private ArrayList<Answer> answersFull;

	public Event(long groupId, long ruleId, long time){
		this.groupId = groupId;
		this.ruleId = ruleId;
		this.time = time;
		answers = new ArrayList<Long>();
	}

	public void addAnswer(Answer add){
		answers.add(add.getId());
	}

	/**
	 * Loads the eventRule to memory from the database. Making it accessible
	 */
	private void loadEventRuleToMemory(){
		er = EventRule.loadFromId(ruleId);
	}

	/**
	 * Loads the full answer to memory from the database
	 */
	private void loadAnswersToMemory(){
		answersFull = new ArrayList<Answer>();
		for(int i = 0; i < answers.size(); i++){
			Answer a = Answer.loadFromId(answers.get(i));
			if(a != null)
				answersFull.add(a);
		}
		Collections.sort(answersFull);
	}

	/**
	 * Checks if the reminder should be sent for this event.
	 * If true a reminder will be sent to users inside maxSize that hasn't answered
	 * reminderSent will be true after reminder has been sent
	 */
	public boolean trySendReminder(){
		boolean sent = false;
		if(!reminderSent){
			if(hasReminderTimePassed()){
				if(answersFull == null)
					loadAnswersToMemory();
				ArrayList<Answer> maxSizeAnswers = getMaxSizeAnswers();
				for(Answer a : maxSizeAnswers){
					if(a.getAnswer() == Answer.UNANSWERED){
						sendReminderToUser(a);
					}
				}
				reminderSent = true;
				sent = true;
				saveToDatastore();
			}
		}
		return sent;
	}

	private void sendReminderToUser(Answer a) {
		User u = User.loadFromId(a.getUserId());
		try {
			Group g = Group.loadFromId(groupId);
			ContactMethod cm = u.getContactMethod(g.getName());
			cm.sendReminderMsg(
					er.getReminderMsg().replace("@@", TimeHelper.longToNiceString_ddMMMHHmm(time - er.getLockTime(), g.getTimeZone())), 
					er.getName(), 
					groupId, 
					id, 
					a.getId());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (GcmError e) {
			u.setContactMethod("mail");
			sendReminderToUser(a);
		}
	}

	/**
	 * Checks if reminderTime has passed
	 * @return
	 */
	public boolean hasReminderTimePassed(){
		if(er == null)
			loadEventRuleToMemory();
		if(System.currentTimeMillis() > time - er.getReminderTime())
			return true;
		return false;
	}

	/**
	 * Checks if lockTime has passed
	 * @return
	 */
	public boolean hasLockEventTimePassed(){
		if(er == null)
			loadEventRuleToMemory();
		if(System.currentTimeMillis() > time - er.getLockTime())
			return true;
		return false;
	}
	
	/**
	 * Checks if this event has passed
	 * @return
	 */
	public boolean hasEventPassed(){
		if(System.currentTimeMillis() > time)
			return true;
		return false;
	}

	/**
	 * Tries to lock the event.
	 * If maxSize is full and the lockTime has passed the list will be locked.
	 * mail will be sent to those that is inside maxSize
	 * @return true if this event was locked by this call
	 */
	public boolean tryLockEvent(){
		boolean locked = false;
		if(!listLocked){
			if(hasLockEventTimePassed()){
				if(answersFull == null)
					loadAnswersToMemory();
				int yesSayers = 0;
				for(Answer a : answersFull){
					if(a.getAnswer() == Answer.UNANSWERED)
						setAnswerNo(a, false);
					else if(a.getAnswer() == Answer.YES)
						yesSayers++;
				}
				if(yesSayers >= er.getMaxSize()){//event is full
					ArrayList<String> partName = new ArrayList<String>();
					for(Answer a : getMaxSizeAnswers())
						partName.add(User.loadFromId(a.getUserId()).getName());
					String groupName = Group.loadFromId(groupId).getName();
					if(er == null)
						loadEventRuleToMemory();
					for(Answer a : getMaxSizeAnswers()){
						sendLockedMsgToUser(partName, groupName, a);
					}
					listLocked = true;
					locked = true;
					saveToDatastore();
				}
				else{//event is not full
					if(!freeSpotOnLockReminderSent){//send mail to no sayers if we haven't already done so
						for(Answer a : answersFull){
							if(a.getAnswer() == Answer.NO)
								mailFreeSpot(a);
						}
						freeSpotOnLockReminderSent = true;
						saveToDatastore();
					}
				}
			}
		}
		return locked;
	}

	private void sendLockedMsgToUser(ArrayList<String> partName, String groupName, Answer a){
		User u = User.loadFromId(a.getUserId());
		try {
			ContactMethod cm = u.getContactMethod(Group.loadFromId(groupId).getName());
			cm.sendLockedListMsg(this, groupName, er.getName(), partName);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (GcmError e) {
			u.setContactMethod("mail");
			sendLockedMsgToUser(partName, groupName, a);
		}
	}

	/**
	 * Gets the full answerobjects for this event.
	 * Loads the answers to memory 
	 * @return
	 */
	public ArrayList<Answer> getAnswers(){
		if(answersFull == null)
			loadAnswersToMemory();
		return answersFull;
	}

	/**
	 * Gets the eventRule for this event
	 * @return
	 */
	public EventRule getEventRule(){
		if(er == null)
			loadEventRuleToMemory();
		return er;
	}

	/**
	 * Turns this object to json
	 * @param timeZone
	 * @param light If it should be a light JSONObject with fewer parameters
	 * @param the requsting users id, so that the users answer can be added. If light = true this param is not used
	 * @return
	 * @throws JSONException
	 */
	public JSONObject toJson(TimeZone timeZone, boolean light, long requstingUserId) throws JSONException{
		JSONObject json = new JSONObject();
		if(er == null)
			loadEventRuleToMemory();
		json.put("date", TimeHelper.createTime(time, timeZone));
		json.put("name", er.getName());
		json.put("id", id);
		if(!light){
			json.put("priorityList", er.priorityListToArray());
			json.put("answers", answerListToArray());
			json.put("maxSize", er.getMaxSize());
			Answer a = null;
			for(Answer as : answersFull){
				if(as.getUserId() == requstingUserId){
					a = as;
					break;
				}
			}
			json.put("requstingUserAnswer", a == null ? null : a.toJson());
			json.put("lockParticipants", TimeHelper.createTime((time - er.getLockTime()), timeZone));
			json.put("okToAnswer", !listLocked);
		}
		return json;
	}

	private JSONArray answerListToArray() throws JSONException{
		if(answersFull == null)
			loadAnswersToMemory();
		JSONArray ja = new JSONArray();
		for(int i = 0; i < answersFull.size(); i++){
			ja.put(answersFull.get(i).toJson());
		}
		return ja;
	}

	/**
	 * Returns the points that the user has from this event.
	 * Takes care of if the user said yes but couldn't participate because of maxSize
	 * @param userId
	 * @return the participationPoint for this event, negative if the user participated else positive
	 */
	public int getParticipationPointsForUser(long userId){
		if(didUserParticipateInEvent(userId))
			return -getEventRule().getParticipationPoint();
		return getEventRule().getParticipationPoint();
	}

	/**
	 * Sets the answer and updates position based on the answer
	 * @param a
	 * @param willParticipate
	 * @throws AnswerNotPartOfEventException 
	 */
	public void setAnswer(Answer a, boolean willParticipate) throws AnswerNotPartOfEventException{
		if(answersFull == null)
			loadAnswersToMemory();
		Collections.sort(answersFull);
		boolean answerIsPartOfThisEvent = false;
		for(Answer an : answersFull){
			if(an.compareTo(a) == 0){
				answerIsPartOfThisEvent = true;
				break;
			}
		}
		if(!answerIsPartOfThisEvent)
			throw new AnswerNotPartOfEventException("The answer is not part of this event");
		if(willParticipate)
			setAnswerYes(a);
		else
			setAnswerNo(a, true);
	}

	private void setAnswerYes(Answer a) {
		a.setAnswer(Answer.YES);
		for(int i = a.getPlace() - 1; i >= 0; i--){
			Answer an = answersFull.get(i);
			if(an.getAnswer() == Answer.NO){
				an.setPlace(i + 1);
				an.saveToDatastore();
				a.setPlace(i);
			}
			else
				break;
		}
		a.saveToDatastore();
		tryLockEvent();
	}

	private void setAnswerNo(Answer a, boolean sendMailToNext) {
		a.setAnswer(Answer.NO);
		for(Answer an : answersFull){
			if(an.getPlace() > a.getPlace()){
				an.setPlace(an.getPlace() - 1);
				an.saveToDatastore();
				if(sendMailToNext){
					if(er == null)
						loadEventRuleToMemory();
					if(an.getPlace() == er.getMaxSize() - 1){//send mail if this answer got inside the maxRange
						if(an.getAnswer() == Answer.UNANSWERED){//only mail if the user hasn't answered already
							mailFreeSpot(an);
						}
					}
				}
			}
		}
		a.setPlace(answersFull.size() - 1);
		a.saveToDatastore();
	}

	private void mailFreeSpot(Answer an) {
		User u = User.loadFromId(an.getUserId());
		try {
			Group g = Group.loadFromId(groupId);
			ContactMethod cm = u.getContactMethod(g.getName());
			cm.sendFreeSpotMsg(er.getName(), 
					er.getAvailableSpotMsg().replace("@@", TimeHelper.longToNiceString_ddMMMHHmm(time, g.getTimeZone())), 
					groupId, 
					id,
					an.getId());
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (GcmError e) {
			u.setContactMethod("mail");
			mailFreeSpot(an);
		}
	}

	/**
	 * Gets the answer at the position
	 * @param pos
	 * @return null if there is no answer at that position
	 */
	public Answer getAnswerAtPosition(int pos){
		if(answersFull == null)
			loadAnswersToMemory();
		else
			Collections.sort(answersFull);
		if(pos < 0)
			pos = 0;
		if(pos > answersFull.size() - 1)
			pos = answersFull.size() - 1;
		return answersFull.get(pos);
	}

	/**
	 * Chekcs if the user participated in this event
	 * Takes care of if the user said yes but couldn't participate because of maxSize
	 * @param userId
	 * @return if the user participated or not in this event
	 */
	public boolean didUserParticipateInEvent(long userId){
		if(answersFull == null)
			loadAnswersToMemory();
		int yesCounter = 0;
		for(int i = 0; i < answersFull.size(); i++){
			Answer a = answersFull.get(i);
			if(a.getUserId() == userId){
				if(a.getAnswer() == Answer.NO)
					return false; //Didn't participate
				else{
					if(yesCounter >= getEventRule().getMaxSize())
						return false; //Said yes but there was not room
					return true; //Said yes and there was room
				}
			}
			else{
				if(!(a.getAnswer() == Answer.NO))
					yesCounter++;	//another said yes
			}
		}
		return false; //Was not in this event
	}

	/**
	 * Saves or updates this object in the datastore
	 */
	public void saveToDatastore(){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if(id == 0){
			Entity event = new Entity(DATASTORE_NAME);
			setEntityParams(event);
			ds.put(event);
			id = event.getKey().getId();
		}
		else{
			Key k = KeyFactory.createKey(DATASTORE_NAME, id);
			Entity group = new Entity(k);
			setEntityParams(group);
			ds.put(group);
		}
	}

	private void setEntityParams(Entity group){
		group.setProperty("groupId", groupId);
		group.setProperty("ruleId", ruleId);
		group.setProperty("time", time);
		group.setProperty("answers", answers);
		group.setProperty("listLocked", listLocked);
		group.setProperty("reminderSent", reminderSent);
		group.setProperty("freeSpotOnLockReminderSent", freeSpotOnLockReminderSent);
	}

	/**
	 * Finds the latest eventTime for rule in the group
	 * @param groupId
	 * @param ruleId
	 * @return 0 if there is no events for the rule in that group
	 */
	public static long latestEventTime(long groupId, long ruleId){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Filter gId = new Query.FilterPredicate("groupId", Query.FilterOperator.EQUAL, groupId);
		Filter rId = new Query.FilterPredicate("ruleId", Query.FilterOperator.EQUAL, ruleId);
		Filter and = CompositeFilterOperator.and(gId, rId);

		Query q = new Query(DATASTORE_NAME).setFilter(and);

		PreparedQuery pq = ds.prepare(q);
		long latest = 0;
		for(Entity e : pq.asIterable()){
			if(latest < (Long)e.getProperty("time"))
				latest = (Long)e.getProperty("time");
		}
		return latest;
	}

	/**
	 * Loads all events for a group from a startDate to an enddate
	 * @param groupId
	 * @param start
	 * @param end
	 * @return
	 */
	public static ArrayList<Event> getEvents(long groupId, long start, long end){
		ArrayList<Event> events = new ArrayList<Event>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Filter gId = new Query.FilterPredicate("groupId", Query.FilterOperator.EQUAL, groupId);
		Filter greaterThanStart = new Query.FilterPredicate("time", Query.FilterOperator.GREATER_THAN_OR_EQUAL, start);
		Filter lessThanEnd = new Query.FilterPredicate("time", Query.FilterOperator.LESS_THAN_OR_EQUAL, end);
		Filter and = CompositeFilterOperator.and(gId, greaterThanStart, lessThanEnd);

		Query q = new Query(DATASTORE_NAME).setFilter(and);
		PreparedQuery pq = ds.prepare(q);
		for(Entity e : pq.asIterable()){
			Event ev = Event.eventFromEntity(e);
			if(ev != null)
				events.add(ev);
		}
		return events;
	}

	/**
	 * Gets all events that has reminderSent set to false
	 * @return
	 */
	public static ArrayList<Event> getEventsWithReminderFalse(){
		return getEventsWithBooleanNameFalse("reminderSent");
	}
	
	/**
	 * Gets all events that has listLocked set to false
	 * @return
	 */
	public static ArrayList<Event> getEventsWithListLockedFalse(){
		return getEventsWithBooleanNameFalse("listLocked");
	}
	
	private static ArrayList<Event> getEventsWithBooleanNameFalse(String booleanName){
		ArrayList<Event> events = new ArrayList<Event>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Filter reminderFalse = new Query.FilterPredicate(booleanName, Query.FilterOperator.EQUAL, false);

		Query q = new Query(DATASTORE_NAME).setFilter(reminderFalse);
		PreparedQuery pq = ds.prepare(q);
		for(Entity e : pq.asIterable()){
			Event ev = Event.eventFromEntity(e);
			if(ev != null)
				events.add(ev);
		}
		return events;
	}

	/**
	 * Gets the answers with position <= maxSize for this event
	 * used to mail the initial users of this event
	 * @return
	 */
	public ArrayList<Answer> getMaxSizeAnswers(){
		if(answersFull == null)
			loadAnswersToMemory();
		if(er == null)
			loadEventRuleToMemory();
		ArrayList<Answer> mailList = new ArrayList<Answer>();
		for(Answer a : answersFull){
			// place starts at 0
			if(a.getPlace() < er.getMaxSize())
				mailList.add(a);
		}
		return mailList;
	}

	/**
	 * Loads an event from an id
	 * @param id
	 * @return
	 */
	public static Event loadFromId(long id){
		Key k = KeyFactory.createKey(DATASTORE_NAME, id);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try {
			Entity result = ds.get(k);
			Event g = eventFromEntity(result);
			return g;
		} catch (Exception returnNull) {}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static Event eventFromEntity(Entity result){
		Event e = new Event((Long)result.getProperty("groupId"),
				(Long)result.getProperty("ruleId"),
				(Long)result.getProperty("time"));
		e.listLocked = (Boolean)result.getProperty("listLocked");
		e.reminderSent = (Boolean)result.getProperty("reminderSent");
		e.freeSpotOnLockReminderSent = (Boolean)result.getProperty("freeSpotOnLockReminderSent");
		List<Long> load = (List<Long>)result.getProperty("answers");
		if(load != null)
			e.answers = load;
		e.id = result.getKey().getId();
		return e;
	}

	public long getRuleId(){
		return ruleId;
	}

	public long getTime(){
		return time;
	}

	public long getId(){
		return id;
	}

	public long getGroupId(){
		return groupId;
	}

	public boolean isListLocked() {
		return listLocked;
	}

	public int compareTo(Event o) {
		if(time > o.time)
			return 1;
		if(time < o.time)
			return -1;
		return 0;
	}
	
	public void setListLocked(boolean locked){
		listLocked = locked;
	}
}
