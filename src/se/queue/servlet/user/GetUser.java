package se.queue.servlet.user;

import java.io.IOException;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;


public class GetUser extends MainServlet{
	private static final long serialVersionUID = 4990549936336985422L;


	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");

		checkParameter(token, "token");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			if(u != null){
				try {
					res.setContentType("application/json");
					out.print(u.toJson(false));
				} catch (JSONException e) {
					set500Error(e.getMessage());
				}
			}
			else{
				set404Error("user was not found");
			}
		}
		out.close();
	}
}
