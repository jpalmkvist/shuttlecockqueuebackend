package se.queue.servlet.rule;

import java.io.IOException;
import java.net.URLDecoder;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyRuleMsg extends MainServlet{
	private static final long serialVersionUID = 6358003892760721112L;

	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] ruleId = req.getParameterValues("ruleId");
		
		//optional
		String[] creationMsg = req.getParameterValues("creationMsg");
		String[] reminderMsg = req.getParameterValues("reminderMsg");
		String[] availableSpotMsg = req.getParameterValues("availableSpotMsg");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(ruleId, "ruleId");

		if(!paramFailed){
			User admin = User.loadWithToken(token[0]);
			try{
				long gId = Long.parseLong(groupId[0]);
				long rId = Long.parseLong(ruleId[0]);
				if(admin != null){
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(admin)){
							EventRule er = EventRule.loadFromId(rId);
							if(er != null){
								if(creationMsg != null){
									logger.warning("creation: " + URLDecoder.decode(creationMsg[0], "UTF-8"));
									logger.warning("creation: " + creationMsg[0]);
									er.setCreationMsg(creationMsg[0]);
								}
								if(reminderMsg != null)
									er.setReminderMsg(reminderMsg[0]);
								if(availableSpotMsg != null)
									er.setAvailableSpotMsg(availableSpotMsg[0]);
								er.saveToDatastore();
							}
							else{
								set404Error("rule was not found");
							}
						}
						else{
							set403Error("You are not allowed to modify users for this group");
						}
					}
					else{
						set404Error("group was not found");
					}
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}