package se.queue.servlet.user;

import java.io.IOException;

import se.exception.WrongParametersException;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class CreateUser extends MainServlet{
	private static final long serialVersionUID = 8814526985232480516L;

	public void handleRequest() throws IOException {
		String[] name = req.getParameterValues("userName");
		String[] pass = req.getParameterValues("password");
		String[] mail = req.getParameterValues("mail");

		checkParameter(name, "name");
		checkParameter(pass, "pass");
		checkParameter(mail, "mail");

		if(!paramFailed){
			String lowerMail = mail[0].toLowerCase();
			if(User.isMailFree(lowerMail)){
				User u;
				try {
					u = new User(name[0], pass[0], lowerMail);
					u.generateToken();
					u.saveToDatastore();
					se.queue.model.Login l = new se.queue.model.Login(u);
					res.setContentType("application/json");
					out.print(l.toJson());
				} catch (WrongParametersException e) {
					set400Error(e.getMessage());
				} catch (JSONException e) {
					set500Error(e.getMessage());
				}
			}
			else
				set400Error("Mail is already in use");
		}
		out.close();
	}
}
