package se.queue.servlet.rule;

import java.io.IOException;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyRuleRepeat extends MainServlet{
	private static final long serialVersionUID = 6358003892760721112L;

	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] ruleId = req.getParameterValues("ruleId");
		String[] repeatTime = req.getParameterValues("repeatTime");
		String[] repeatTimeType = req.getParameterValues("repeatTimeType");
		String[] paused = req.getParameterValues("paused");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(ruleId, "ruleId");
		checkParameter(repeatTime, "repeatTime");
		checkParameter(repeatTimeType, "repeatTimeType");
		checkParameter(paused, "paused");

		if(!paramFailed){
			User admin = User.loadWithToken(token[0]);
			try{
				long gId = Long.parseLong(groupId[0]);
				long rId = Long.parseLong(ruleId[0]);
				if(admin != null){
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(admin)){
							EventRule er = EventRule.loadFromId(rId);
							if(er != null){
								er.setRepeatTime(Integer.parseInt(repeatTime[0]));
								er.setRepeatTimeType(repeatTimeType[0]);
								er.setPaused(Boolean.parseBoolean(paused[0]));
								er.saveToDatastore();
							}
							else{
								set404Error("rule was not found");
							}
						}
						else{
							set403Error("You are not allowed to modify users for this group");
						}
					}
					else{
						set404Error("group was not found");
					}
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}