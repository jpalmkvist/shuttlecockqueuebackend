package se.exception;

@SuppressWarnings("serial")
public class AnswerNotPartOfEventException extends Exception{
	public AnswerNotPartOfEventException(String msg){
		super(msg);
	}

}
