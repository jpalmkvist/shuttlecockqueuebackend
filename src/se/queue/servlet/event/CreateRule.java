
package se.queue.servlet.event;

import java.io.IOException;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;
import se.queue.utility.TimeHelper;

public class CreateRule extends MainServlet{
	private static final long serialVersionUID = -1983533152151655671L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] firstTime = req.getParameterValues("firstTime");
		String[] repeatTime = req.getParameterValues("repeatTime");
		String[] repeatTimeType = req.getParameterValues("repeatTimeType");
		String[] maxSize = req.getParameterValues("maxSize");
		String[] eventName = req.getParameterValues("eventName");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(firstTime, "firstTime");
		checkParameter(repeatTime, "repeatTime");
		checkParameter(repeatTimeType, "repeatTimeType");
		checkParameter(maxSize, "maxSize");
		checkParameter(eventName, "eventName");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			if(u != null){
				try {
					long gId = Long.parseLong(groupId[0]);
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(u)){
							long firstL = TimeHelper.stringToLong(firstTime[0], g.getTimeZone());
							int repeatL = Integer.parseInt(repeatTime[0]);
							int maxL = Integer.parseInt(maxSize[0]);
							//beh�vs detta? millis �r fortfarande samma...
							//Calendar cal = g.getCalendarInGroupTimeZone();
							//cal.setTimeInMillis(firstL);
							//logger.warning(firstL + " intime: " + TimeHelper.longToString(firstL, g.getTimeZone()));
							//logger.warning(cal.getTimeInMillis() + " calTime: " + TimeHelper.longToString(cal.getTimeInMillis(), g.getTimeZone()));
							
							EventRule er = new EventRule(firstL, repeatL, EventRule.parseString(repeatTimeType[0]), maxL, eventName[0]);
							er.saveToDatastore();
							g.addRule(er.getId());
							g.saveToDatastore();
							res.setContentType("application/json");
							out.print(er.toJson(g.getTimeZone(), false));
						}
						else
							set403Error("You are not allowed to modify this group");
					}
					else
						set404Error("group does not exist");
				} catch (Exception e) {
					logger.warning("error creating rule: " + e.getMessage());
					set400Error(e.getMessage());
				}
			}
			else
				set404Error("token does not exist");
		}
		out.close();
	}
}
