package se.queue.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class MainServlet extends HttpServlet{
	private static final long serialVersionUID = 6643499955985475688L;
	protected HttpServletRequest req;
	protected HttpServletResponse res;
	protected PrintWriter out;
	protected boolean paramFailed = false;
	protected Logger logger;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		setupHandling(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		setupHandling(req, res);
	}
	
	private void setupHandling(HttpServletRequest req, HttpServletResponse res) throws IOException{
		logger = Logger.getLogger("mLogger");
		this.req = req;
		this.res = res;
		req.setCharacterEncoding("UTF-8");
		res.setCharacterEncoding("UTF-8");
		out = res.getWriter();
		paramFailed = false;
		handleRequest();
		
	}

	protected abstract void handleRequest() throws IOException;
	
	/**
	 * Checks that the parameter is not null or zero length
	 * If check fails, status is set to 400 and an errorMsg is printed to out. 
	 * The parameter will be translated as UTF-8 encoded
	 * paramFailed will be true if this check fails
	 * @param param
	 * @param paramName
	 * @return
	 */
	protected boolean checkParameter(String[] param, String paramName){
		if(param != null){
			if(param[0].length() != 0){
				logger.warning("checking: " + paramName + ": " + param[0]);
				return true;
			}
		}
		logger.warning("ParamError: " + paramName + " missing. ");
		set400Error("ParamError: " + paramName + " missing. ");
		paramFailed = true;
		return false;
	}
	
	/**
	 * Bad request
	 * Missing param ans such
	 */
	protected void set400Error(String msg){
		res.setStatus(400);
		out.print(msg);
	}
	
	/**
	 * Unauthorized
	 * Non admin changing groups etc
	 */
	protected void set403Error(String msg){
		res.setStatus(403);
		out.print(msg);
	}
	
	/**
	 * Missing object serverside
	 * User dont exist, group dont exist...
	 */
	protected void set404Error(String msg){
		res.setStatus(404);
		out.print(msg);
	}
	
	/**
	 * Conflict
	 * answering to events that is locked
	 * @param msg
	 */
	protected void set409Error(String msg) {
		res.setStatus(409);
		out.print(msg);
	}
	
	/**
	 * Internal error
	 */
	protected void set500Error(String msg){
		res.setStatus(500);
		out.print(msg);
	}
}
