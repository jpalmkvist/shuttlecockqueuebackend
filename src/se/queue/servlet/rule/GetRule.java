package se.queue.servlet.rule;

import java.io.IOException;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class GetRule extends MainServlet{
	private static final long serialVersionUID = 6737713001992274179L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] ruleId = req.getParameterValues("ruleId");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(ruleId, "ruleId");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			long gId = Long.parseLong(groupId[0]);
			long rId = Long.parseLong(ruleId[0]);
			if(u != null){
				Group g = Group.loadFromId(gId);
				if(g != null){
					if(g.isUserAdmin(u)){
						EventRule er = EventRule.loadFromId(rId);
						if(er != null){
							try {
								res.setContentType("application/json");
								out.print(er.toJson(g.getTimeZone(), false));
							} catch (JSONException ex) {
								set500Error(ex.getMessage());
							}
						}
						else{
							set404Error("eventRule was not found");
						}
					}
					else{
						set403Error("user is not authorized to get this object");
					}
				}
				else{
					set404Error("group was not found");
				}
			}
			else{
				set404Error("user was not found");
			}
		}
		out.close();
	}
}
