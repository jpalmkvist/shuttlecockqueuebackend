package se.queue.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import se.exception.WrongParametersException;
import se.queue.utility.TimeHelper;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class EventRule {
	private static final String DATASTORE_NAME = "EventRule";
	public enum RepeatTimeType{DAY, WEEK, MONTH, YEAR}
	private static final int MIN_NAME = 2, MAX_NAME = 20;
	private long 	id, 			//database id
					firstTime,		//first occurrence of this event
					creationTime,	//time before next event the event will be created
					reminderTime,	//time before lock that users that hasn't answered will be reminded
					lockTime;		//time before the event that the participants will be locked. Unanswered will be no. Notifications will be sent to all users who said no.
	
	private int repeatTime, 		//how often this event will be repeated
				participationPoint, //points for participation
				priorityPoint, 		//the prioritized users extra points for this event
				maxSize;			//how many spots this event has
	private RepeatTimeType repeatTimeType; 	//the type that will be used to repeat this event
	private List<Long> priorityUsers;//Users that are prioritized for this event
	private boolean paused;			//if the event is paused for the time, no new events will be created
	private String 	eventName,		//Nice name for the event
					creationMsg,	//Message that will be sent to the users when the event is created
					reminderMsg,	//Reminder message for users that hasn't answered before the reminderTime
					availableSpotMsg;	//Message to all users who haven't answered that there is an available spot for this event
	
	public EventRule(long firstTime, int repeatTime, RepeatTimeType repeatTimeType, int maxSize, String eventName) throws WrongParametersException{
		this.firstTime = firstTime;
		creationTime = 1000l * 60 * 60 * 24 * 2; 	//2 days default
		reminderTime = 1000l * 60 * 60 * 6; 		//6 hours default
		lockTime = 1000l * 60 * 60 * 2; 			//2 hours default
		this.repeatTime = repeatTime;
		this.repeatTimeType = repeatTimeType;
		participationPoint = 2;						//2 points default
		priorityPoint = 10;							//10 points default
		this.maxSize = maxSize;
		paused = false;
		//events = new ArrayList<Event>();
		priorityUsers = new ArrayList<Long>();
		setEventName(eventName);
		creationMsg = eventName + ": time to answer. Event at @@";
		reminderMsg = eventName + ": you haven't answered yet. List will be locked at @@. If you dont answer your place will be available to others";
		availableSpotMsg = eventName + ": has a free spot. Event at @@";
	}
	
	public JSONObject toJson(TimeZone groupZone, boolean light) throws JSONException{
		JSONObject json = new JSONObject();
		json.put("id", id);
		json.put("startDate", TimeHelper.createTime(firstTime, groupZone));
		json.put("repeatTime", repeatTime);
		json.put("repeatTimeType", enumRepeatToString());
		json.put("eventName", eventName);
		json.put("paused", paused);
		if(!light){
			json.put("creationTime", creationTime);
			json.put("reminderTime", reminderTime);
			json.put("lockTime", lockTime);
			json.put("participationPoint", participationPoint);
			json.put("priorityPoint", priorityPoint);
			json.put("maxSize", maxSize);
			json.put("priorityUsers", priorityListToArray());
			json.put("creationMsg", creationMsg);
			json.put("reminderMsg", reminderMsg);
			json.put("availableSpotMsg", availableSpotMsg);
		}
		return json;
	}
	
	public JSONArray priorityListToArray() throws JSONException{
		JSONArray ja = new JSONArray();
		for(int i = 0; i < priorityUsers.size(); i++){
			try{
				ja.put(User.loadFromId(priorityUsers.get(i)).toJson(true));
			}catch(NullPointerException ignore){}
		}
		return ja;
	}
	
	/**
	 * Saves or updates this object in the datastore
	 */
	public void saveToDatastore(){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if(id == 0){
			Entity rule = new Entity(DATASTORE_NAME);
			setEntityParams(rule);
			ds.put(rule);
			id = rule.getKey().getId();
		}
		else{
			Key k = KeyFactory.createKey(DATASTORE_NAME, id);
			Entity rule = new Entity(k);
			setEntityParams(rule);
			ds.put(rule);
		}
	}
	
	private void setEntityParams(Entity rule){
		rule.setProperty("firstTime", firstTime);
		rule.setProperty("creationTime", creationTime);
		rule.setProperty("reminderTime", reminderTime);
		rule.setProperty("lockTime", lockTime);
		rule.setProperty("repeatTime", repeatTime);
		rule.setProperty("participationPoint", participationPoint);
		rule.setProperty("priorityPoint", priorityPoint);
		rule.setProperty("maxSize", maxSize);
		rule.setProperty("repeatTimeType", enumRepeatToString());
		rule.setProperty("priorityUsers", priorityUsers);
		rule.setProperty("paused", paused);
		rule.setProperty("eventName", eventName);
		rule.setProperty("creationMsg", creationMsg);
		rule.setProperty("reminderMsg", reminderMsg);
		rule.setProperty("availableSpotMsg", availableSpotMsg);
	}
	
	/**
	 * Loads the rule from the id
	 * @param id
	 * @return null if there is no rule with this id
	 */
	@SuppressWarnings("unchecked")
	public static EventRule loadFromId(long id){
		Key k = KeyFactory.createKey(DATASTORE_NAME, id);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try {
			Entity result = ds.get(k);
			EventRule g = new EventRule((Long)result.getProperty("firstTime"),
					((Long)result.getProperty("repeatTime")).intValue(),
					parseString((String)result.getProperty("repeatTimeType")),
					((Long)result.getProperty("maxSize")).intValue(),
					(String)result.getProperty("eventName"));
			g.creationTime = (Long)result.getProperty("creationTime");
			g.reminderTime = (Long)result.getProperty("reminderTime");
			g.lockTime = (Long)result.getProperty("lockTime");
			g.participationPoint = ((Long)result.getProperty("participationPoint")).intValue();
			g.priorityPoint = ((Long)result.getProperty("priorityPoint")).intValue();
			g.paused = (Boolean)result.getProperty("paused");
			g.creationMsg = (String)result.getProperty("creationMsg");
			g.reminderMsg = (String)result.getProperty("reminderMsg");
			g.availableSpotMsg = (String)result.getProperty("availableSpotMsg");
			
			List<Long> load = (List<Long>)result.getProperty("priorityUsers");
			if(load != null)
				g.priorityUsers = load;
			g.id = result.getKey().getId();
			return g;
		} catch (Exception returnNull) {}
		return null;
	}
	
	public void setEventName(String eventName) throws WrongParametersException {
		if(eventName.length() >= MIN_NAME && eventName.length() <= MAX_NAME)
			this.eventName = eventName;
		else
			throw new WrongParametersException("groupname must be between " + MIN_NAME + " and " + MAX_NAME +" characters");
	}
	
	public static RepeatTimeType parseString(String type){
		if(type.compareToIgnoreCase("day") == 0)
			return RepeatTimeType.DAY;
		if(type.compareToIgnoreCase("week") == 0)
			return RepeatTimeType.WEEK;
		if(type.compareToIgnoreCase("month") == 0)
			return RepeatTimeType.MONTH;
		if(type.compareToIgnoreCase("year") == 0)
			return RepeatTimeType.YEAR;
		return RepeatTimeType.WEEK;
	}
	
	private String enumRepeatToString(){
		if(repeatTimeType == RepeatTimeType.DAY)
			return "day";
		if(repeatTimeType == RepeatTimeType.WEEK)
			return "week";
		if(repeatTimeType == RepeatTimeType.MONTH)
			return "month";
		return "year";
	}
	
	/**
	 * Gets the first time that this event happened
	 * @return
	 */
	public long getFirstTime(){
		return firstTime;
	}
	
	public long getReminderTime(){
		return reminderTime;
	}
	
	public void addUserToPriority(long userId){
		if(!userIsPriority(userId))
			priorityUsers.add(userId);
	}
	
	public void removeUserFromPriority(long userId){
		if(userIsPriority(userId))
			priorityUsers.remove(userId);
	}
	
	public boolean userIsPriority(long userId){
		boolean exists = false;
		for(int i = 0; i < priorityUsers.size(); i++){
			if(priorityUsers.get(i) == userId){
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	public int getRepeatTime(){
		return repeatTime;
	}
	
	public long getCreationTime(){
		return creationTime;
	}
	
	/**
	 * Gets the repeatTimeType in CalendarForm
	 * Calendar.DAY, MONTH etc
	 * @return
	 */
	public int getRepeatTimeTypeInCalendar(){
		if(repeatTimeType == RepeatTimeType.DAY)
			return Calendar.DAY_OF_YEAR;
		else if(repeatTimeType == RepeatTimeType.WEEK)
			return Calendar.WEEK_OF_YEAR;
		else if(repeatTimeType == RepeatTimeType.MONTH)
			return Calendar.MONTH;
		return Calendar.YEAR;
	}
	
	public long getId(){
		return id;
	}
	
	public String getName(){
		return eventName;
	}
	
	public int getMaxSize(){
		return maxSize;
	}
	
	public long getLockTime(){
		return lockTime;
	}
	
	public boolean getPaused(){
		return paused;
	}
	
	public int getParticipationPoint(){
		return participationPoint;
	}
	
	public List<Long> getPriorityList(){
		return priorityUsers;
	}
	
	public int getPriorityPoint(){
		return priorityPoint;
	}

	public void setCreationMsg(String creationMsg) {
		this.creationMsg = creationMsg;
	}

	public void setReminderMsg(String reminderMsg) {
		this.reminderMsg = reminderMsg;
	}

	public void setAvailableSpotMsg(String availableSpotMsg) {
		this.availableSpotMsg = availableSpotMsg;
	}
	
	public void setRepeatTime(int repeatTime){
		this.repeatTime = repeatTime;
	}
	
	public void setRepeatTimeType(String parseAbleType){
		repeatTimeType = parseString(parseAbleType);
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public void setFirstTime(long firstTime) {
		this.firstTime = firstTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public void setReminderTime(long reminderTime) {
		this.reminderTime = reminderTime;
	}

	public void setLockTime(long lockTime) {
		this.lockTime = lockTime;
	}

	public void setParticipationPoint(int participationPoint) {
		this.participationPoint = participationPoint;
	}

	public void setPriorityPoint(int priorityPoint) {
		this.priorityPoint = priorityPoint;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public String getCreationMsg() {
		return creationMsg;
	}

	public String getReminderMsg() {
		return reminderMsg;
	}

	public String getAvailableSpotMsg() {
		return availableSpotMsg;
	}
	
	
}
