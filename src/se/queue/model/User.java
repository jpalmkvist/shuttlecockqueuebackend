package se.queue.model;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.geronimo.mail.util.Base64;

import se.exception.WrongParametersException;
import se.queue.utility.ContactMethod;
import se.queue.utility.GcmHelper;
import se.queue.utility.Mail;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class User {
	private static final int MIN_NAME = 2, MAX_NAME = 20;
	private static final String MAIL_EXP = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
	public static final String DATASTORE_NAME = "User";
	public static final int CONTACT_MAIL = 0, 
			CONTACT_ANDROID = 1,
			CONTACT_IOS = 2;

	private String name, pass;
	private long id;
	private String token;
	private String mail, appid;
	private List<Long> groups;
	private int contactMethod;

	public User(String name, String pass, String mail) throws WrongParametersException{
		setName(name);
		setPass(pass);
		setMail(mail);
		appid = "";
		contactMethod = CONTACT_MAIL;
		groups = new ArrayList<Long>();
	}

	/**
	 * Saves or updates this object in the datastore
	 */
	public void saveToDatastore(){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if(id == 0){
			Entity user = new Entity(DATASTORE_NAME);
			setEntityParams(user);
			ds.put(user);
			id = user.getKey().getId();
		}
		else{
			Key k = KeyFactory.createKey(DATASTORE_NAME, id);
			Entity user = new Entity(k);
			setEntityParams(user);
			ds.put(user);
		}
	}

	private void setEntityParams(Entity user){
		user.setProperty("name", name);
		user.setProperty("pass", pass);
		user.setProperty("token", token);
		user.setProperty("mail", mail);
		user.setProperty("appid", appid);
		user.setProperty("contactMethod", contactMethod);
		user.setProperty("groups", groups);
	}
	
	public ContactMethod getContactMethod(String subject) throws UnsupportedEncodingException, MessagingException{
		if(contactMethod == CONTACT_ANDROID)
			return new GcmHelper(this, subject);
		return new Mail(this, subject);
	}

	/**
	 * Loads a user from the datastore with the token
	 * @param token
	 * @return
	 */
	public static User loadWithToken(String token){
		return loadFromDataStore("token", token, Query.FilterOperator.EQUAL);
	}

	/**
	 * Loads a user from the id
	 * @param id
	 * @return null if there is no user with the id
	 */
	public static User loadFromId(Long id){
		Key k = KeyFactory.createKey(DATASTORE_NAME, id);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try {
			Entity result = ds.get(k);
			return userFromEntity(result);
		} catch (Exception returnNull) {}
		return null;
	}

	/**
	 * Loads a user by the mailadress the validates the password.
	 * @param mail
	 * @param pass
	 * @return The user with mail and pass. returns null if the mail doesnt exist or the pass doesnt match
	 */
	public static User loadWithMailAndPass(String mail, String pass){
		User u = loadFromDataStore("mail", mail, Query.FilterOperator.EQUAL);
		if(u != null){
			if(u.validatePass(pass))
				return u;
		}
		return null;
	}

	/**
	 * Checks if a mailadress is unused
	 * @param mail
	 * @return
	 */
	public static boolean isMailFree(String mail){
		if(loadFromDataStore("mail", mail, Query.FilterOperator.EQUAL) == null)
			return true;
		return false;
	}

	private static User loadFromDataStore(String propertyKey, String propertyValue, Query.FilterOperator filter){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(DATASTORE_NAME).setFilter(new Query.FilterPredicate(propertyKey, filter, propertyValue));
		PreparedQuery pq = ds.prepare(q);
		Entity result = pq.asSingleEntity();
		if(result != null){
			try {
				return userFromEntity(result);
			} catch (WrongParametersException ignore) {
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static User userFromEntity(Entity result) throws WrongParametersException{
		User u = new User((String)result.getProperty("name"), (String)result.getProperty("pass"), (String)result.getProperty("mail"));
		u.token = (String)result.getProperty("token");
		u.id = result.getKey().getId();
		u.appid = (String)result.getProperty("appid");
		u.contactMethod = ((Long)result.getProperty("contactMethod")).intValue();
		List<Long> load = (List<Long>)result.getProperty("groups");
		if(load != null)
			u.groups = load;
		return u;
	}

	/**
	 * Creates a JsonObject from this object
	 * @return
	 * @throws JSONException
	 */
	public JSONObject toJson(boolean light) throws JSONException{
		JSONObject json = new JSONObject();
		json.put("name", name);
		json.put("id", id);
		json.put("mail", mail);
		if(!light){
			//json.put("token", token);
			json.put("contactMethod", getContactString());
			json.put("groupList", groupsToArray());
		}
		return json;
	}
	
	private String getContactString(){
		if(contactMethod == CONTACT_MAIL)
			return "mail";
		else if(contactMethod == CONTACT_ANDROID)
			return "android";
		else
			return "ios";
	}
	
	/**
	 * Sets the contactMethod based on the given string
	 * Defaults to "mail"
	 * @param method Acceptable Strings: "mail", "android", "ios"
	 */
	public void setContactMethod(String method){
		if(method.compareTo("mail") == 0)
			contactMethod = CONTACT_MAIL;
		else if(method.compareTo("android") == 0)
			contactMethod = CONTACT_ANDROID;
		/* TODO support IOS
		else if(method.compareTo("ios") == 0)
			contactMethod = CONTACT_IOS;*/
		else
			contactMethod = CONTACT_MAIL;
	}
	
	public int getContactMethod(){
		return contactMethod;
	}
	
	/**
	 * Sets the appId to the id from Google Cloud Messaging
	 * @param gcmId
	 */
	public void setAppId(String gcmId){
		appid = gcmId;
	}
	
	/**
	 * Gets the Google Cloud Messaging id
	 * @return
	 */
	public String getAppId(){
		return appid;
	}

	private JSONArray groupsToArray() throws JSONException{
		JSONArray ja = new JSONArray();
		// sort by latest first
		for(int i = groups.size() - 1; i >= 0; i--) {
			try{
				ja.put(Group.loadFromId(groups.get(i)).toJson(this, true));
			}catch(NullPointerException ignore){}
		}
		return ja;
	}

	/**
	 * Adds a group to this user. If the user already is a member nothing happens
	 * @param g id of the group
	 */
	public void addGroup(long g){
		boolean alreadyMember = false;
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i) == g){
				alreadyMember = true;
				break;
			}
		}
		if(!alreadyMember)
			groups.add(g);
	}

	/**
	 * Removes the group from this user
	 * @param g
	 */
	public void removeGroup(long g){
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i) == g){
				groups.remove(i);
				break;
			}
		}
	}

	/**
	 * Gets a list of the groups this user is part of
	 * @return
	 */
	public List<Long> getGroups(){
		return groups;
	}

	/**
	 * Searches the database for user matching the searchString
	 * Will search in userName and mail
	 * @param search
	 * @return
	 */
	public static ArrayList<User> searchForUser(String search){
		ArrayList<User> list = new ArrayList<User>();

		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(DATASTORE_NAME);
		PreparedQuery pq = ds.prepare(q);
		for (Entity result : pq.asIterable()) {
			try {
				User u = User.userFromEntity(result);
				if(u.contains(search))
					list.add(u);
			} catch (WrongParametersException ignore) {}
		}
		return list;
	}
	
	/**
	 * Loads all users in the datastore
	 * @return
	 */
	public static ArrayList<User> loadAllUsers(){
		ArrayList<User> list = new ArrayList<User>();

		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(DATASTORE_NAME);
		PreparedQuery pq = ds.prepare(q);
		for (Entity result : pq.asIterable()) {
			try {
				User u = User.userFromEntity(result);
				list.add(u);
			} catch (WrongParametersException ignore) {}
		}
		return list;
	}

	/**
	 * Checks if the User contains the String
	 * Will check userName and Mail
	 * @param s
	 * @return
	 */
	private boolean contains(String s){
		boolean contains = false;
		contains = name.toLowerCase().contains(s.toLowerCase());
		if(!contains)
			contains = mail.toLowerCase().contains(s.toLowerCase());
		return contains;
	}

	/**
	 * Generates a new token that will be at least 30 chars
	 */
	public void generateToken(){
		StringBuilder sb = new StringBuilder();
		int randoms = (int)(Math.random() * 5);
		for(int i = 0; i < randoms; i++)
			sb.append(generateRandomChar());
		sb.append(mail);
		for(int i = sb.length(); i < 30; i++)
			sb.append(generateRandomChar());
		StringBuilder sb2 = new StringBuilder();
		for(int i = 0; i < sb.length(); i++){
			char c = sb.charAt(i);
			if(Math.random() > 0.5)
				c++;
			else
				c--;
			if(c > 125)
				c = 125;
			if(c < 33)
				c = 33;
			sb2.append(c);
		}
		token = Base64.encode(sb2.toString().getBytes()).toString();
	}

	private char generateRandomChar(){
		//33 - 125
		return (char)((Math.random() * 92) + 33);
	}

	/**
	 * Checks if this user is part of the group
	 * @param groupId Id for the group
	 * @return
	 */
	/*public boolean isUserPartOfGroup(long groupId){
		boolean ret = false;
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i) == groupId){
				ret = true;
				break;
			}
		}
		return ret;
	}*/

	/**
	 * Validates that the given password is the same as the stored password
	 * @param pass
	 * @return
	 */
	public boolean validatePass(String pass){
		return this.pass.compareTo(pass) == 0 ? true : false;
	}

	public void setPass(String pass){
		this.pass = pass;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) throws WrongParametersException {
		if(name.length() >= MIN_NAME && name.length() <= MAX_NAME)
			this.name = name;
		else
			throw new WrongParametersException("username must be between " + MIN_NAME + " and " + MAX_NAME +" characters");
	}
	public long getId() {
		return id;
	}
	public String getToken() {
		return token;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) throws WrongParametersException {
		if(mail.matches(MAIL_EXP))
			this.mail = mail;
		else
			throw new WrongParametersException(mail + " is not a correct mailadress");
	}
	
	public String getPass(){
		return pass;
	}
}
