package se.queue.servlet.cron;

import java.io.IOException;
import java.util.ArrayList;

import se.queue.model.Event;
import se.queue.servlet.MainServlet;

public class ReminderService extends MainServlet{
	private static final long serialVersionUID = -1612900605153360900L;

	public void handleRequest() throws IOException {
		long tick = System.currentTimeMillis();
		int reminderSent = 0;
		ArrayList<Event> events = Event.getEventsWithReminderFalse();
		for(Event e : events){
			if(e.trySendReminder())
				reminderSent++;
		}
		logger.warning("Found " + events.size() + " events. " +
				"Sent reminders to " + reminderSent + " events. " +
				"Time: " + (System.currentTimeMillis() - tick) + " ms");
		out.close();
	}
}