package se.queue.servlet.user;

import java.io.IOException;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyUser extends MainServlet{
	private static final long serialVersionUID = 6358003892760721112L;

	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");

		//optional
		String[] userName = req.getParameterValues("userName");
		String[] mail = req.getParameterValues("mail");

		checkParameter(token, "token");

		if(!paramFailed){
			User user = User.loadWithToken(token[0]);
			try{
				if(user != null){
					if(userName != null){
						user.setName(userName[0]);
					}
					if(mail != null){
						if(User.isMailFree(mail[0]))
							user.setMail(mail[0]);
						else
							set409Error("Mail is already in use");
					}
					user.saveToDatastore();
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}