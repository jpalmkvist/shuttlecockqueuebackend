package se.queue.servlet.rule;

import java.io.IOException;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyRuleSettings extends MainServlet{
	private static final long serialVersionUID = 6358003892760721112L;

	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] ruleId = req.getParameterValues("ruleId");
		
		//optional
		String[] reminderTime = req.getParameterValues("reminderTime");
		String[] lockTime = req.getParameterValues("lockTime");
		String[] creationTime = req.getParameterValues("creationTime");
		String[] participationPoint = req.getParameterValues("participationPoint");
		String[] priorityPoint = req.getParameterValues("priorityPoint");
		String[] maxSize = req.getParameterValues("maxSize");
		String[] eventName = req.getParameterValues("eventName");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(ruleId, "ruleId");

		if(!paramFailed){
			User admin = User.loadWithToken(token[0]);
			try{
				long gId = Long.parseLong(groupId[0]);
				long rId = Long.parseLong(ruleId[0]);
				if(admin != null){
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(admin)){
							EventRule er = EventRule.loadFromId(rId);
							if(er != null){
								if(reminderTime != null)
									er.setReminderTime(Long.parseLong(reminderTime[0]));
								if(lockTime != null)
									er.setLockTime(Long.parseLong(lockTime[0]));
								if(creationTime != null)
									er.setCreationTime(Long.parseLong(creationTime[0]));
								if(participationPoint != null)
									er.setParticipationPoint(Integer.parseInt(participationPoint[0]));
								if(priorityPoint != null)
									er.setPriorityPoint(Integer.parseInt(priorityPoint[0]));
								if(maxSize != null)
									er.setMaxSize(Integer.parseInt(maxSize[0]));
								if(eventName != null)
									er.setEventName(eventName[0]);
								er.saveToDatastore();
							}
							else{
								set404Error("rule was not found");
							}
						}
						else{
							set403Error("You are not allowed to modify users for this group");
						}
					}
					else{
						set404Error("group was not found");
					}
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}