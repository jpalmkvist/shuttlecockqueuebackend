package se.queue.servlet.group;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.exception.WrongParametersException;
import se.queue.model.Group;
import se.queue.model.User;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class CreateGroup extends HttpServlet{
	private static final long serialVersionUID = 6643499955985475688L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		handleRequest(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		handleRequest(req, res);
	}

	public void handleRequest(HttpServletRequest req, HttpServletResponse res) throws IOException {

		PrintWriter out = res.getWriter();

		String[] token = req.getParameterValues("token");
		String[] name = req.getParameterValues("name");

		if(token == null || name == null){
			res.setStatus(400);
			out.print("missing parameters");
		}
		else{
			User u = User.loadWithToken(token[0]);
			if(u != null){
				try {
					Group g = new Group(name[0]);
					g.addUserAsAdmin(u.getId());
					g.saveToDatastore();
					u.addGroup(g.getId());
					u.saveToDatastore();

					res.setContentType("application/json");
					out.print(g.toJson(u, false));
				} catch (WrongParametersException e) {
					res.setStatus(400);
					out.print(e.getMessage());
				} catch (JSONException e) {
					res.setStatus(500);
					out.print(e.getMessage());
				}
			}
			else{
				res.setStatus(400);
				out.print("token does not exist");
			}
		}
		out.close();
	}
}
