package se.queue.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import se.exception.TooFewAdminException;
import se.exception.WrongParametersException;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class Group {
	private static final int MIN_NAME = 2, MAX_NAME = 20;
	public static final String DATASTORE_NAME = "Group";
	private String name, timeZone;
	private long id;
	private int eventsBack; //amount of events to go back when deciding who should be in what place when creating new events
	private List<Long> users, admins, lowPriorityUsers, eventRules, events;
	//eventlist, burningEvent

	public Group(String name) throws WrongParametersException{
		setName(name);
		users = new ArrayList<Long>();
		admins = new ArrayList<Long>();
		lowPriorityUsers = new ArrayList<Long>();
		eventRules = new ArrayList<Long>();
		events = new ArrayList<Long>();
		timeZone = "Europe/Stockholm";
		eventsBack = 4;
	}

	public JSONObject toJson(User askingUser, boolean light) throws JSONException{
		JSONObject json = new JSONObject();
		json.put("name", name);
		json.put("id", id);
		if(!light){
			json.put("timeZone", timeZone);
			json.put("eventsBack", eventsBack);
			json.put("userList", usersToArray(users));
			json.put("adminList", usersToArray(admins));
			json.put("lowPriorityUserList", usersToArray(lowPriorityUsers));
			json.put("eventList", eventsToArray(0, (System.currentTimeMillis() + (1000l * 60* 60 * 24 * 30))));
			boolean admin = false;
			for(int i = 0; i < admins.size(); i++){
				if(admins.get(i) == askingUser.getId()){
					admin = true;
					break;
				}
			}
			json.put("adminRights", admin);
		}
		return json;
	}
	
	private JSONArray usersToArray(List<Long> users) throws JSONException{
		JSONArray ja = new JSONArray();
		for(Long g : users){
			try{
				ja.put(User.loadFromId(g).toJson(true));
			}catch(NullPointerException ignore){}
		}
		return ja;
	}
	
	private JSONArray eventsToArray(long start, long end) throws JSONException{
		ArrayList<Event> events = Event.getEvents(id, start, end);
		JSONArray ja = new JSONArray();
		for(Event e : events){
			try{
				ja.put(e.toJson(getTimeZone(), true, -1));
			}catch(NullPointerException ignore){}
		}
		return ja;
	}

	/**
	 * Saves or updates this object in the datastore
	 */
	public void saveToDatastore(){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if(id == 0){
			Entity group = new Entity(DATASTORE_NAME);
			setEntityParams(group);
			ds.put(group);
			id = group.getKey().getId();
		}
		else{
			Key k = KeyFactory.createKey(DATASTORE_NAME, id);
			Entity group = new Entity(k);
			setEntityParams(group);
			ds.put(group);
		}
	}
	
	private void setEntityParams(Entity group){
		group.setProperty("name", name);
		group.setProperty("users", users);
		group.setProperty("admins", admins);
		group.setProperty("eventsBack", eventsBack);
		group.setProperty("timeZone", timeZone);
		group.setProperty("lowPriorityUsers", lowPriorityUsers);
		group.setProperty("eventRules", eventRules);
		group.setProperty("events", events);
	}

	/**
	 * Loads a group from the id
	 * @param id Database id for the group
	 * @return null if there is no group with the id
	 */
	public static Group loadFromId(long id){
		Key k = KeyFactory.createKey(DATASTORE_NAME, id);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try {
			Entity result = ds.get(k);
			Group g = groupFromEntity(result);
			return g;
		} catch (Exception returnNull) {}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private static Group groupFromEntity(Entity result) throws WrongParametersException{
		Group g = new Group((String)result.getProperty("name"));
		g.timeZone = (String)result.getProperty("timeZone");
		g.eventsBack = ((Long)result.getProperty("eventsBack")).intValue();
		List<Long> load = (List<Long>)result.getProperty("users");
		if(load != null)
			g.users = load;
		load = (List<Long>)result.getProperty("admins");
		if(load != null)
			g.admins = load;
		load = (List<Long>)result.getProperty("lowPriorityUsers");
		if(load != null)
			g.lowPriorityUsers = load;
		load = (List<Long>)result.getProperty("eventRules");
		if(load != null)
			g.eventRules = load;
		load = (List<Long>)result.getProperty("events");
		if(load != null)
			g.events = load;
		g.id = result.getKey().getId();
		return g;
	}

	/**
	 * Adds a user as normal user for this group.
	 * Removes the user from admins or lowPriorityUsers
	 * @param user
	 * @throws TooFewAdminException 
	 */
	public void addUserAsUser(long user) throws TooFewAdminException{
		boolean removed = removeUserFromList(admins, user);
		if(!removed)
			removeUserFromList(lowPriorityUsers, user);
		if(!userExistInList(users, user))
			users.add(user);
	}


	/**
	 * Adds a user as a low-priority-user for this group.
	 * Removes the user from admins or users
	 * @param user
	 * @throws TooFewAdminException 
	 */
	public void addUserAsLowPriorityUser(long user) throws TooFewAdminException{
		boolean removed = removeUserFromList(admins, user);
		if(!removed)
			removeUserFromList(users, user);
		if(!userExistInList(lowPriorityUsers, user))
			lowPriorityUsers.add(user);
	}

	/**
	 * Adds a user as admin for this group.
	 * Removes the user from users or lowPriorityUsers
	 * @param user
	 */
	public void addUserAsAdmin(long user){
		boolean removed;
		try {
			removed = removeUserFromList(users, user);
			if(!removed)
				removeUserFromList(lowPriorityUsers, user);
		} catch (TooFewAdminException ignore) {}
		if(!userExistInList(admins, user))
			admins.add(user);
	}

	private boolean removeUserFromList(List<Long> list, long user) throws TooFewAdminException{
		boolean removed = false;
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).compareTo(user) == 0){
				if(list == admins)
					if(admins.size() == 1)
						throw new TooFewAdminException("You can not remove the last administrator");
				list.remove(i);
				removed = true;
				break;
			}
		}
		return removed;
	}
	
	/**
	 * Loads all groups in the datastore
	 * @return
	 */
	public static ArrayList<Group> loadAllGroups(){
		ArrayList<Group> groups = new ArrayList<Group>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(Group.DATASTORE_NAME);
		PreparedQuery pq = ds.prepare(q);
		Iterable<Entity> it = pq.asIterable();
		Iterator<Entity> iterator = it.iterator();
		while(iterator.hasNext()){
			Entity g = iterator.next();
			try {
				Group group = Group.groupFromEntity(g);
				if(group != null)
					groups.add(group);
			} catch (WrongParametersException e) {}
		}
		return groups;
	}
	
	/**
	 * Checks if the user exists in the list already
	 * @param list
	 * @param user
	 * @return
	 */
	private boolean userExistInList(List<Long> list, long user){
		boolean exists = false;
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).compareTo(user) == 0){
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	/**
	 * Removes the user from this group
	 * @param user Id for the user
	 * @throws TooFewAdminException
	 */
	public void removeUserFromGroup(long user) throws TooFewAdminException{
		boolean removed = removeUserFromList(admins, user);
		if(!removed)
			removed = removeUserFromList(users, user);
		if(!removed)
			removed = removeUserFromList(lowPriorityUsers, user);
	}
	
	/**
	 * Loads all eventrules from the datasroe and returns them in an ArrayList
	 * @return
	 */
	public ArrayList<EventRule> getRules(){
		ArrayList<EventRule> rules = new ArrayList<EventRule>();
		for(int i = 0; i < eventRules.size(); i++){
			EventRule er = EventRule.loadFromId(eventRules.get(i));
			if(er != null)
				rules.add(er);
		}
		return rules;
	}
	
	/**
	 * Gets the first time for every eventrule
	 * if now is earlier than all other times, now will be returned instead
	 * @return
	 */
	public long getFirstCreationTimeOfRules(){
		long first = System.currentTimeMillis();
		ArrayList<EventRule> rules = getRules();
		for(EventRule er : rules){
			if(first > er.getFirstTime())
				first = er.getFirstTime();
		}
		return first;
	}
	
	/**
	 * Gets the latest time this event took place
	 * @param ruleId the ruleId for the event
	 * @return 0 if there hasnt been any event yet
	 */
	public long getLastEventTimeFromRuleId(long ruleId){
		return Event.latestEventTime(id, ruleId);
	}

	/**
	 * Checks if the User is admin for this group
	 * @param u
	 * @return
	 */
	public boolean isUserAdmin(User u){
		return userExistInList(admins, u.getId());
	}
	
	/**
	 * Checks if the user is part of this group
	 * @param u
	 * @return
	 */
	public boolean isUserPartOfGroup(User u){
		boolean ret = userExistInList(users, u.getId());
		if(!ret)
			ret = userExistInList(lowPriorityUsers, u.getId());
		if(!ret)
			ret = userExistInList(admins, u.getId());
		return ret;
	}
	
	/**
	 * Adds a rule to this group
	 * @param ruleId
	 */
	public void addRule(long ruleId){
		eventRules.add(ruleId);
	}
	
	/**
	 * Generates answers to the event.
	 * Should be called once, when the event just has been created
	 * @param e
	 */
	public void generateAnswersToEvent(Event e){
		AnswerGenerator ag = new AnswerGenerator(eventsBack, this, e);
		ag.addListToUsers(admins);
		ag.addListToUsers(users);
		ag.addListToStandins(lowPriorityUsers);
		ag.generate();
	}
	
	/**
	 * TEST
	 * @return
	 */
	public ArrayList<Long> getAllusers(){
		ArrayList<Long> u = new ArrayList<Long>();
		u.addAll(users);
		u.addAll(admins);
		u.addAll(lowPriorityUsers);
		return u;
	}

	public String getName() {
		return name;
	}
	
	/**
	 * Gets the timeZone for this group
	 * @return
	 */
	public TimeZone getTimeZone(){
		return TimeZone.getTimeZone(timeZone);
	}
	
	public void setName(String name) throws WrongParametersException {
		if(name.length() >= MIN_NAME && name.length() <= MAX_NAME)
			this.name = name;
		else
			throw new WrongParametersException("groupname must be between " + MIN_NAME + " and " + MAX_NAME +" characters");
	}
	public long getId() {
		return id;
	}
}
