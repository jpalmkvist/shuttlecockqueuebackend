package se.queue.utility;

import java.util.ArrayList;

import javax.mail.MessagingException;

import se.exception.GcmError;
import se.queue.model.Event;

public interface ContactMethod {
	public void sendCreationMsg(String eventRuleName, String creationMsg, long groupId, long eventId, long answerId) throws MessagingException, GcmError;
	public void sendReminderMsg(String reminderMsg, String eventRuleName, long groupId, long eventId, long answerId) throws MessagingException, GcmError;
	public void sendLockedListMsg(Event e, String groupName, String eventRuleName, ArrayList<String> partList) throws MessagingException, GcmError;
	public void sendFreeSpotMsg(String eventRuleName, String freeSpotMsg, long groupId, long eventId, long answerId) throws MessagingException, GcmError;
}
