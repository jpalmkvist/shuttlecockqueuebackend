package se.queue.servlet.group;

import java.io.IOException;

import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class GetGroup extends MainServlet{
	private static final long serialVersionUID = -7715727500835898019L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			long id = Long.parseLong(groupId[0]);
			if(u != null){
				Group g = Group.loadFromId(id);
				if(g != null){
					if(g.isUserPartOfGroup(u)){
						try {
							res.setContentType("application/json");
							out.print(g.toJson(u, false));
						} catch (JSONException e) {
							set500Error(e.getMessage());
						}
					}
					else{
						set403Error("user can not see this group");
					}
				}
				else{
					set404Error("group was not found");
				}
			}
			else{
				set404Error("user was not found");
			}
		}
		out.close();
	}
}
