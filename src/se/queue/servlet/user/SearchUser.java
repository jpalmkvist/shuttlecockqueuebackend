package se.queue.servlet.user;

import java.io.IOException;
import java.util.ArrayList;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class SearchUser extends MainServlet{
	private static final long serialVersionUID = 818021201383155337L;


	public void handleRequest() throws IOException {

		String[] search = req.getParameterValues("search");
		checkParameter(search, "search");

		if(!paramFailed){
			JSONObject json = new JSONObject();
			JSONArray ja = new JSONArray();
			try {
				json.put("users", ja);
				ArrayList<User> list = User.searchForUser(search[0]);
				for(int i = 0; i < list.size(); i++)
					ja.put(list.get(i).toJson(true));
				res.setContentType("application/json");
				out.print(json.toString());
			} catch (JSONException e) {
				set500Error(e.getMessage());
			}
		}
		out.close();
	}
}
