package se.queue.servlet.cron;

import java.io.IOException;
import java.util.ArrayList;

import se.queue.model.Event;
import se.queue.servlet.MainServlet;

public class LockEventService extends MainServlet{
	private static final long serialVersionUID = 3333109581289014447L;

	public void handleRequest() throws IOException {
		long tick = System.currentTimeMillis();
		int eventsLocked = 0;
		ArrayList<Event> events = Event.getEventsWithListLockedFalse();
		for(Event e : events){
			if(e.tryLockEvent())
				eventsLocked++;
			else if(e.hasEventPassed()){
				e.setListLocked(true);
				e.saveToDatastore();
			}
		}
		logger.warning("Found " + events.size() + " events. " +
				"Locked " + eventsLocked + " events. " +
				"Time: " + (System.currentTimeMillis() - tick) + " ms");
		out.close();
	}
}
