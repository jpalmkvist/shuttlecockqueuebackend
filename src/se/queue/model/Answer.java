package se.queue.model;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class Answer implements Comparable<Answer>{
	public static final String DATASTORE_NAME = "Answer";
	public static final int 	YES = 0,
								NO = 1,
								UNANSWERED = 2;
	private long id, user;
	private int place,			//first place = 0, starts at zero 
				answer, calcPoints;
	
	public Answer(long user, int place){
		this.user = user;
		this.place = place;
		answer = UNANSWERED;
	}
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		User u = User.loadFromId(user);
		json.put("user", u.toJson(true));
		json.put("id", id);
		String a = "";
		switch(answer){
		case YES:
			a = "yes";
			break;
		case NO:
			a = "no";
			break;
		case UNANSWERED:
			a = "unanswered";
			break;
		}
		json.put("answer", a);
		json.put("points", calcPoints);
		json.put("place", place);
		return json;
	}
	
	/**
	 * DO NOT CALL THIS DIRECTLY
	 * should be called from Event.setAnswer
	 * @param answer
	 */
	void setAnswer(int answer){
		this.answer = answer;
	}
	
	public void setCalcPoints(int points){
		calcPoints = points;
	}
	
	/**
	 * Saves or updates this object in the datastore
	 */
	public void saveToDatastore(){
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if(id == 0){
			Entity answer = new Entity(DATASTORE_NAME);
			setEntityParams(answer);
			ds.put(answer);
			id = answer.getKey().getId();
		}
		else{
			Key k = KeyFactory.createKey(DATASTORE_NAME, id);
			Entity answer = new Entity(k);
			setEntityParams(answer);
			ds.put(answer);
		}
	}
	
	private void setEntityParams(Entity answer){
		answer.setProperty("user", user);
		answer.setProperty("place", place);
		answer.setProperty("answer", this.answer);
		answer.setProperty("calcPoints", calcPoints);
	}
	
	public static Answer loadFromId(long id){
		Key k = KeyFactory.createKey(DATASTORE_NAME, id);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try {
			Entity result = ds.get(k);
			Answer a = answerFromEntity(result);
			return a;
		} catch (Exception returnNull) {}
		return null;
	}
	
	private static Answer answerFromEntity(Entity result){
		Answer a = new Answer(
				(Long)result.getProperty("user"),
				((Long)result.getProperty("place")).intValue());
		a.answer = ((Long)result.getProperty("answer")).intValue();
		a.calcPoints = ((Long)result.getProperty("calcPoints")).intValue();
		a.id = result.getKey().getId();
		return a;
	}
	
	/**
	 * DO NOT CALL
	 * Should only be called from Event.setAnswer
	 * @param place
	 */
	void setPlace(int place){
		this.place = place;
	}

	public long getId() {
		return id;
	}
	
	public long getUserId(){
		return user;
	}
	
	public int getAnswer(){
		return answer;
	}

	public int getPlace(){
		return place;
	}
	public int compareTo(Answer o) {
		return place - o.place;
	}
}
