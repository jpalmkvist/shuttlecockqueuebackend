package se.queue.servlet.rule;

import java.io.IOException;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyRuleUser extends MainServlet{
	private static final long serialVersionUID = 6358003892760721112L;
	private static final String ADD_USER = "add", REMOVE = "remove";


	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] ruleId = req.getParameterValues("ruleId");
		String[] userId = req.getParameterValues("userId");
		String[] action = req.getParameterValues("action");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(ruleId, "ruleId");
		checkParameter(userId, "userId");
		checkParameter(action, "action");

		if(!paramFailed){
			User admin = User.loadWithToken(token[0]);
			try{
				long gId = Long.parseLong(groupId[0]);
				long rId = Long.parseLong(ruleId[0]);
				long uId = Long.parseLong(userId[0]);
				User user = User.loadFromId(uId);
				if(user != null && admin != null){
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(admin)){
							EventRule er = EventRule.loadFromId(rId);
							if(er != null){
								if(action[0].compareTo(ADD_USER) == 0){
									er.addUserToPriority(user.getId());
								}
								else if(action[0].compareTo(REMOVE) == 0){
									er.removeUserFromPriority(user.getId());
								}
								else{
									set404Error("unknown action");
								}
								er.saveToDatastore();
							}
							else{
								set404Error("rule was not found");
							}
						}
						else{
							set403Error("You are not allowed to modify users for this group");
						}
					}
					else{
						set404Error("group was not found");
					}
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}
