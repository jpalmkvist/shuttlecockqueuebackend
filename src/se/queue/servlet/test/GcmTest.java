package se.queue.servlet.test;

import java.io.IOException;

import javax.mail.MessagingException;

import se.exception.GcmError;
import se.queue.model.User;
import se.queue.servlet.MainServlet;
import se.queue.utility.ContactMethod;

@SuppressWarnings("serial")
public class GcmTest extends MainServlet{

	@Override
	protected void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");

		checkParameter(token, "token");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			//u = User.loadWithToken("[B@8f17ca");
			try {
				ContactMethod c = u.getContactMethod("tja");
				c.sendReminderMsg("remind", "ruleName", 5393792802750464l, 5696459148099584l, 0);
				out.print("{\"reminder\":true}");
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (GcmError e) {
				e.printStackTrace();
			}
		}
		out.close();
		
	}

}
