package se.queue.servlet.user;

import java.io.IOException;

import com.google.appengine.labs.repackaged.org.json.JSONException;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class SetPassword extends MainServlet{
	private static final long serialVersionUID = -5248991566940984475L;

	@Override
	protected void handleRequest() throws IOException {


		String[] token = req.getParameterValues("token");
		String[] oldpass = req.getParameterValues("oldpass");
		String[] newpass = req.getParameterValues("newpass");
		

		checkParameter(token, "token");
		checkParameter(oldpass, "oldpass");
		checkParameter(newpass, "newpass");
		

		if(!paramFailed){
			User user = User.loadWithToken(token[0]);
			if(user != null){
				if(user.validatePass(oldpass[0])){
					user.setPass(newpass[0]);
					user.generateToken();
					user.saveToDatastore();
					se.queue.model.Login l = new se.queue.model.Login(user);
					try {
						res.setContentType("application/json");
						out.print(l.toJson());
					} catch (JSONException e) {
						set500Error(e.getMessage());
					}
				}
				else
					set403Error("old password is incorrect.");
			}
			else{
				set404Error("user was not found");
			}
		}
	}

}
