package se.queue.servlet.group;

import java.io.IOException;

import se.exception.TooFewAdminException;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class ModifyUserGroup extends MainServlet{
	private static final long serialVersionUID = -7496891335582219523L;
	private static final String ADD_USER = "addUser", ADD_LOW = "addLowPriority", ADD_ADMIN = "addAdmin", REMOVE = "remove";


	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] userId = req.getParameterValues("userId");
		String[] action = req.getParameterValues("action");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(userId, "userId");
		checkParameter(action, "action");

		if(!paramFailed){
			User admin = User.loadWithToken(token[0]);
			try{
				long gId = Long.parseLong(groupId[0]);
				long uId = Long.parseLong(userId[0]);
				User user = User.loadFromId(uId);
				if(user != null && admin != null){
					Group g = Group.loadFromId(gId);
					if(g != null){
						if(g.isUserAdmin(admin)){
							try{
								if(action[0].compareTo(ADD_USER) == 0){
									g.addUserAsUser(user.getId());
									user.addGroup(gId);
								}
								else if(action[0].compareTo(ADD_LOW) == 0){
									g.addUserAsLowPriorityUser(user.getId());
									user.addGroup(gId);
								}
								else if(action[0].compareTo(ADD_ADMIN) == 0){
									g.addUserAsAdmin(user.getId());
									user.addGroup(gId);
								}
								else if(action[0].compareTo(REMOVE) == 0){
									g.removeUserFromGroup(user.getId());
									user.removeGroup(gId);
								}
								else{
									set404Error("unknown action");
								}
								g.saveToDatastore();
								user.saveToDatastore();
							}catch(TooFewAdminException e){
								set403Error(e.getMessage());
							}
						}
						else{
							set403Error("You are not allowed to modify users for this group");
						}
					}
					else{
						set404Error("group was not found");
					}
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}
