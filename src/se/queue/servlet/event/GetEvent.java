package se.queue.servlet.event;

import java.io.IOException;

import se.queue.model.Event;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class GetEvent extends MainServlet{
	private static final long serialVersionUID = -6382992834202041826L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] eventId = req.getParameterValues("eventId");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(eventId, "eventId");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			long gId = Long.parseLong(groupId[0]);
			long eId = Long.parseLong(eventId[0]);
			if(u != null){
				Group g = Group.loadFromId(gId);
				if(g != null){
					if(g.isUserPartOfGroup(u)){
						Event e = Event.loadFromId(eId);
						if(e != null){
							try {
								res.setContentType("application/json");
								out.print(e.toJson(g.getTimeZone(), false, u.getId()));
							} catch (JSONException ex) {
								set500Error(ex.getMessage());
							}
						}
						else{
							set404Error("event was not found");
						}
					}
					else{
						set403Error("user can not see this group");
					}
				}
				else{
					set404Error("group was not found");
				}
			}
			else{
				set404Error("user was not found");
			}
		}
		out.close();
	}
}
