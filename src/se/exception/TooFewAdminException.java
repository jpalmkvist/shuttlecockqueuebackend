package se.exception;

public class TooFewAdminException extends Exception{
	private static final long serialVersionUID = 1189847918489081886L;

	public TooFewAdminException(String msg){
		super(msg);
	}
}
