package se.queue.servlet.group;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import se.queue.model.Answer;
import se.queue.model.Event;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetGroupStatistic extends MainServlet {
	private static final long serialVersionUID = -6382992834202041826L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");

		if (!paramFailed) {
			User u = User.loadWithToken(token[0]);
			long gId = Long.parseLong(groupId[0]);
			if (u != null) {
				Group g = Group.loadFromId(gId);
				if (g != null) {
					if (g.isUserPartOfGroup(u)) {
						ArrayList<Event> events = Event.getEvents(gId, 0,
								System.currentTimeMillis());
						ArrayList<Stat> stats = new ArrayList<GetGroupStatistic.Stat>();

						for (Event e : events) {
							ArrayList<Answer> answers = e.getAnswers();

							for (Answer a : answers) {
								long uId = a.getUserId();
								Stat stat = getStatFromUserId(stats, uId);
								if (a.getAnswer() == Answer.YES) {
									stat.yes++;
								} else {
									stat.no++;
								}

							}
						}

						try {
							res.setContentType("application/json");
							JSONArray jsonStats = new JSONArray();
							int totalEvents = events.size();
							Collections.sort(stats);
							for(Stat s: stats) {
								jsonStats.put(s.toJson(totalEvents));
							}
							out.print(jsonStats);
						} catch (JSONException ex) {
							set500Error(ex.getMessage());
						}
					} else {
						set403Error("user can not see this group");
					}
				} else {
					set404Error("group was not found");
				}
			} else {
				set404Error("user was not found");
			}
		}
		out.close();
	}

	private Stat getStatFromUserId(ArrayList<Stat> stats, long userId) {
		Stat stat = null;
		for (Stat s : stats) {
			if (userId == s.user.getId()) {
				stat = s;
				break;
			}
		}

		if (stat == null) {
			stat = new Stat();
			stat.user = User.loadFromId(userId);
			stats.add(stat);
		}

		return stat;
	}

	private class Stat implements Comparable<Stat>{
		User user;
		int yes;
		int no;
		
		JSONObject toJson(int total) throws JSONException{
			JSONObject stat = new JSONObject();
			stat.put("user", user.toJson(true));
			stat.put("yes", yes);
			stat.put("no", no);
			stat.put("total", total);
			
			return stat;
		}

		public int compareTo(Stat o) {
			return o.yes - yes;
		}
	}

}
