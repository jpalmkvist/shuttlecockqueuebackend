package se.exception;

public class WrongParametersException extends Exception{
	private static final long serialVersionUID = 2224174295055358126L;

	public WrongParametersException(String msg){
		super(msg);
	}
}
