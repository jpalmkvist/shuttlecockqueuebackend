package se.queue.servlet.cron;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.mail.MessagingException;

import se.exception.GcmError;
import se.queue.model.Answer;
import se.queue.model.Event;
import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;
import se.queue.utility.ContactMethod;
import se.queue.utility.TimeHelper;

public class CreateEvents extends MainServlet{
	private static final long serialVersionUID = -1612900605153360900L;
	private int createdEvents = 0;
	private int sentMails = 0;

	public void handleRequest() throws IOException {
		long tick = System.currentTimeMillis();
		createdEvents = 0;
		sentMails = 0;

		ArrayList<Group> groups = Group.loadAllGroups();
		//logger.warning("createEvents found " + groups.size() + " groups");
		for(int i = 0; i < groups.size(); i++){
			Group g = groups.get(i);
			long now = System.currentTimeMillis();
			ArrayList<EventRule> rules = g.getRules();
			//logger.warning("group " + g.getName() + ": " + rules.size() + " rules");
			for(int j = 0; j < rules.size(); j++){
				EventRule er = rules.get(j);
				//generateEachTime(g, er);
				if(er.getRepeatTime() > 0 && !er.getPaused()){
					long lastEventTime = g.getLastEventTimeFromRuleId(er.getId());
					if(lastEventTime < now){
						//logger.warning("lastEvent:");
						//logger.warning(TimeHelper.longToString(lastEvent, g.getTimeZone()));
						if(lastEventTime == 0)
							lastEventTime = er.getFirstTime();
						
						//logger.warning("lastEvent after zero check:");
						//logger.warning(TimeHelper.longToString(lastEventTime, g.getTimeZone()));
						Calendar calLastEvent = Calendar.getInstance();
						calLastEvent.setTimeInMillis(lastEventTime);
						while(calLastEvent.getTimeInMillis() < now){
							calLastEvent.add(er.getRepeatTimeTypeInCalendar(), er.getRepeatTime());
							//logger.warning("adding to: " + TimeHelper.longToString(calLastEvent.getTimeInMillis(), g.getTimeZone()));
						}
						lastEventTime = calLastEvent.getTimeInMillis();
						//logger.warning("lastEvent after whileloop:");
						//logger.warning(TimeHelper.longToString(lastEvent, g.getTimeZone()));
						long creationTime = lastEventTime - er.getCreationTime();;
						//logger.warning("create date:");
						//logger.warning(TimeHelper.longToString(creationTime, g.getTimeZone()));
						//logger.warning(creationTime + "");
						//logger.warning("system date:");
						//logger.warning(TimeHelper.longToString(now, g.getTimeZone()));
						//logger.warning(now + "");
						if(creationTime < now){
							createEvent(g, er, lastEventTime);
						}
						else{
							//logger.warning("should not create this event: " + er.getName());
						}
					}
				}
			}
		}
		logger.warning("Found " + groups.size() + " groups. " +
				"Created " + createdEvents + " events. " +
				"Time: " + (System.currentTimeMillis() - tick) + " ms " + 
				"Sent: " + sentMails + " mails.");
		out.close();
	}

	private void createEvent(Group g, EventRule er, long lastEventTime)
			throws UnsupportedEncodingException {
		Event e = new Event(g.getId(), er.getId(), lastEventTime);
		e.saveToDatastore();//generate id
		g.generateAnswersToEvent(e);
		e.saveToDatastore();
		ArrayList<Answer> maiList = e.getMaxSizeAnswers();
		for(Answer a : maiList){
			sendCreationMsgToUser(g, e, a);
		}
		createdEvents++;
		//logger.warning("should create this event: " + er.getName());
	}

	private void sendCreationMsgToUser(Group g, Event e, Answer a)
			throws UnsupportedEncodingException {
		User u = User.loadFromId(a.getUserId());
		try {
			ContactMethod cm = u.getContactMethod(g.getName());
			cm.sendCreationMsg(e.getEventRule().getName(), 
					e.getEventRule().getCreationMsg().replace("@@", TimeHelper.longToNiceString_ddMMMHHmm(e.getTime(), g.getTimeZone())), 
					g.getId(), 
					e.getId(),
					a.getId());
		    sentMails++;
		} catch (MessagingException e1) {
			logger.warning("MessagingException: " + e1.getMessage());
		} catch (GcmError e1) {
			logger.warning("GcmError: " + e1.getMessage());
			u.setContactMethod("mail");
			sendCreationMsgToUser(g, e, a);
		}
	}
	
	/**
	 * TEST generates events for each call
	 * @param g
	 * @param er
	 * @throws UnsupportedEncodingException 
	 */
	/*
	private void generateEachTime(Group g, EventRule er) throws UnsupportedEncodingException{
		createEvent(g, er, System.currentTimeMillis() + er.getCreationTime());
	}
	*/
}
