package se.queue.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import se.queue.utility.TimeHelper;

public class AnswerGenerator {
	private Event generateTo;
	private EventRule er;
	private int eventsBack;
	private Group group;
	private ArrayList<Long> users, standins;
	private ArrayList<Event> events;
	private ArrayList<PointUser> sortedOnPointsList;
	
	//Test
	private Logger l;

	public AnswerGenerator(int eventsBack, Group group, Event generateTo){
		this.eventsBack = eventsBack;
		this.group = group;
		this.generateTo = generateTo;
		users = new ArrayList<Long>();
		standins = new ArrayList<Long>();
		events = new ArrayList<Event>();
		sortedOnPointsList = new ArrayList<PointUser>();
		l = Logger.getLogger("mLogger");
	}

	public void addListToUsers(List<Long> add){
		users.addAll(add);
	}

	public void addListToStandins(List<Long> add){
		standins.addAll(add);
	}

	/**
	 * Generates answers based on the users in the group and their past participation in earlier events
	 * Saves the generated answers to the datastore
	 */
	public void generate(){
		er = EventRule.loadFromId(generateTo.getRuleId());
		l.warning("loaded eventrule with name: " + er.getName());
		l.warning("loading events from: " + TimeHelper.longToString(group.getFirstCreationTimeOfRules(), TimeZone.getTimeZone("Europe/Stockholm")) +
				"to: " + TimeHelper.longToString(System.currentTimeMillis(), TimeZone.getTimeZone("Europe/Stockholm")));
		events = Event.getEvents(group.getId(), group.getFirstCreationTimeOfRules(), System.currentTimeMillis());
		l.warning("loaded " + events.size() + " events");
		Collections.sort(events);
		for(int i = 0; i < events.size(); i++)
			l.warning("ordered: " + TimeHelper.longToString(events.get(i).getTime(), TimeZone.getTimeZone("Europe/Stockholm")));

		ArrayList<PointUser> pointUsers = new ArrayList<PointUser>();
		l.warning("creating: " + users.size() + " users");
		for(int i = 0; i < users.size(); i++){
			pointUsers.add(new PointUser(users.get(i)));
			l.warning(pointUsers.get(i).user + " has: " + pointUsers.get(i).point + " points");
		}
		
		ArrayList<PointUser> pointStandins = new ArrayList<PointUser>();
		l.warning("creating: " + standins.size() + " standins");
		for(int i = 0; i < standins.size(); i++){
			pointStandins.add(new PointUser(standins.get(i)));
			l.warning(pointStandins.get(i).user + " has: " + pointStandins.get(i).point + " points");
		}
		
		sortPointUsers(pointUsers);
		sortPointUsers(pointStandins);
		for(int i = 0; i < sortedOnPointsList.size(); i++){
			Answer a = new Answer(sortedOnPointsList.get(i).user, i);
			
			a.setCalcPoints(sortedOnPointsList.get(i).point);
			a.saveToDatastore();
			generateTo.addAnswer(a);
		}
	}

	/**
	 * Sorts the users based on points
	 * the user with highest points and tieBreaker will be in place 0 of the sortedOnPointsList
	 * @param list
	 */
	private void sortPointUsers(ArrayList<PointUser> list){
		l.warning("sorting list with " + list.size() + " users");
		while(list.size() != 0){
			l.warning("users " + list.size() + " left to sort");
			int highestPoint = getHighestPointInList(list);
			ArrayList<PointUser> samePoint = moveUsersWithPointsToNewList(list, highestPoint);
			l.warning("found " + samePoint.size() + " users with the highest point: " + highestPoint);
			if(samePoint.size() == 1)
				addListToSorted(samePoint);
			else{
				decideTieBreakerForMultipleUsers(samePoint);
			}
		}
	}

	/**
	 * Tiebreakers are calculated like this:
	 * Users on the same point are checked backwards in previous events one by one.
	 * Users that didnt participate in an event are lifted to a list with other users who didnt participate.
	 * Those users are randomly added to the sorted list.
	 * Users that participated stays in the list and are checked towards other users who also participated in the event
	 * @param samePoint
	 */
	private void decideTieBreakerForMultipleUsers(ArrayList<PointUser> samePoint) {
		int checkedEvents = 1;//begin at one because of index starts with 0
		ArrayList<PointUser> addToSortedList = new ArrayList<PointUser>();
		while(samePoint.size() > 0){
			l.warning("samePoint " + samePoint.size() + " left to sort");
			if(checkedEvents <= events.size()){
				l.warning("getting event " + checkedEvents + " back");
				Event e = events.get(events.size() - checkedEvents);

				for(int i = samePoint.size() - 1; i >= 0; i--){
					PointUser pu = samePoint.get(i);
					if(!e.didUserParticipateInEvent(pu.user)){
						addToSortedList.add(samePoint.get(i));
						samePoint.remove(i);
					}
				}
				if(addToSortedList.size() > 0)
					addListToSorted(addToSortedList);
				checkedEvents++;
			}
			else{
				l.warning("no events left to check");
				addListToSorted(samePoint);
			}
		}
	}

	/**
	 * Adds all the users in the list to the sortedList.
	 * List will be cleared after they are added
	 * @param listWithSamePoints
	 */
	private void addListToSorted(ArrayList<PointUser> listWithSamePoints){
		l.warning("adding " + listWithSamePoints.size() + " to sorted list");
		Collections.sort(listWithSamePoints);
		sortedOnPointsList.addAll(listWithSamePoints);
		listWithSamePoints.clear();
	}


	/**
	 * Finds the highest points in this list
	 * @param list
	 * @return
	 */
	private int getHighestPointInList(ArrayList<PointUser> list) {
		int highestPoint = Integer.MIN_VALUE;
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).point > highestPoint)
				highestPoint = list.get(i).point;
		}
		return highestPoint;
	}

	/**
	 * Moves users from the list with the specified points to a new list
	 * @param list
	 * @param highestPoint
	 * @return
	 */
	private ArrayList<PointUser> moveUsersWithPointsToNewList( ArrayList<PointUser> list, int highestPoint) {
		ArrayList<PointUser> usersOnThisPoint = new ArrayList<PointUser>();
		for(int i = list.size() - 1; i >= 0; i--){
			if(list.get(i).point == highestPoint){
				usersOnThisPoint.add(list.get(i));
				list.remove(i);
			}
		}
		return usersOnThisPoint;
	}

	private class PointUser implements Comparable<PointUser>{
		private long user;
		private int point;
		private double randomTieBreaker;

		private PointUser(long user){
			l.warning("creating points for user: " + user);
			this.user = user;
			point = 0;
			calculatePoints();
			addPriorityPoint();
			randomTieBreaker = Math.random();
			l.warning("endpoints: " + point);
		}

		/**
		 * Calculates points for this user based on participation in the last events(eventsBack)
		 */
		private void calculatePoints(){
			int eventsChecked = 0;
			for(int i = events.size() - 1; i >= 0; i--){
				if(eventsChecked == eventsBack)
					break;
				Event e = events.get(i);
				point += e.getParticipationPointsForUser(user);
				eventsChecked++;
				l.warning("ec: " + eventsChecked + " points: " + point);
			}
		}

		/**
		 * Adds the priorityPoints for this user if the user is in the priorityList of the event
		 */
		private void addPriorityPoint(){
			List<Long> priorityUsers = generateTo.getEventRule().getPriorityList();
			for(int i = 0; i < priorityUsers.size(); i++){
				if(priorityUsers.get(i) == user){
					l.warning("found priority: " + user + " points before: " + point);
					point += generateTo.getEventRule().getPriorityPoint();
					l.warning("found priority: " + user + " points after: " + point);
					break;
				}
			}
		}

		public int compareTo(PointUser o) {
			return ((Double)randomTieBreaker).compareTo(o.randomTieBreaker);
		}
	}
}
