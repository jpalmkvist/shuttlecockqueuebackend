package se.queue.servlet.group;

import java.io.IOException;
import java.util.ArrayList;

import se.queue.model.EventRule;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetEventRules extends MainServlet{
	private static final long serialVersionUID = -7715727500835898019L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			long id = Long.parseLong(groupId[0]);
			if(u != null){
				Group g = Group.loadFromId(id);
				if(g != null){
					if(g.isUserAdmin(u)){
						try {
							ArrayList<EventRule> rules = g.getRules();
							JSONObject json = new JSONObject();
							JSONArray ja = new JSONArray();
							for(int i = 0; i < rules.size(); i++)
								ja.put(rules.get(i).toJson(g.getTimeZone(), true));
							json.put("rules", ja);
							res.setContentType("application/json");
							out.print(json);
						} catch (JSONException e) {
							set500Error(e.getMessage());
						}
					}
					else{
						set403Error("user can not see rules for this group");
					}
				}
				else{
					set404Error("group was not found");
				}
			}
			else{
				set404Error("user was not found");
			}
		}
		out.close();
	}
}
