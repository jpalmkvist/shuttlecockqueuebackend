package se.queue.utility;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import se.queue.model.Event;
import se.queue.model.User;


public class Mail implements ContactMethod{
	private static final String fromAdress = "omadeveloper@gmail.com";
	//private static final String fromPersonal = "DoNotReply Que";
	private Message msg;
	private User receiver;
	
	public Mail(User receiver, String subject) throws UnsupportedEncodingException, MessagingException{
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props);
		msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(fromAdress, subject));
		msg.addRecipient(Message.RecipientType.TO, 
				new InternetAddress(receiver.getMail(), receiver.getName()));
		/*Logger l = Logger.getLogger("mLogger");
		l.warning("MAIL: receiver: " + receiver.getName() + " mail: " + receiver.getMail());;*/
		this.receiver = receiver;
		
	}
	
	private void setSubject(String subject) throws MessagingException{
		msg.setSubject(subject);
	}

	
	private void setMultiPartMessage(Multipart mp) throws MessagingException{
		msg.setContent(mp);
	}
	
	/**
	 * Sets the message for the creation of an event.
	 * Links to event and answers are added
	 * @throws MessagingException 
	 */
	public void sendCreationMsg(String eventRuleName, String creationMsg, long groupId, long eventId, long answerId) throws MessagingException{
		setSubject(creationMsg);
		Multipart mp = new MimeMultipart();

        MimeBodyPart htmlPart = new MimeBodyPart();
        String htmlBody = receiver.getName() + "<br>" +
				creationMsg +
				getEventLink(groupId, eventId, eventRuleName) +
		        "<br><br>" +
		        "Will you be participating in this event?" +
		        "<br>" +
		        getAnswerLink(true, groupId, eventId, answerId)+
		        "<br>" +
		        getAnswerLink(false, groupId, eventId, answerId);
        htmlPart.setContent(htmlBody, "text/html");		
        mp.addBodyPart(htmlPart);
        setMultiPartMessage(mp);
        send();
	}
	
	public void sendReminderMsg(String reminderMsg, String eventRuleName, long groupId, long eventId, long answerId) throws MessagingException{
		setSubject(reminderMsg);
		Multipart mp = new MimeMultipart();

        MimeBodyPart htmlPart = new MimeBodyPart();
        String htmlBody = receiver.getName() + "<br>" +
        		reminderMsg +
        		"<br>" +
				getEventLink(groupId, eventId, eventRuleName) +
		        "<br><br>" +
		        "Will you be participating in this event?" +
		        "<br>" +
		        getAnswerLink(true, groupId, eventId, answerId)+
		        "<br>" +
		        getAnswerLink(false, groupId, eventId, answerId);
        htmlPart.setContent(htmlBody, "text/html");		
        mp.addBodyPart(htmlPart);
        setMultiPartMessage(mp);
        send();
	}
	
	public void sendFreeSpotMsg(String eventRuleName, String freeSpotMsg, long groupId, long eventId, long answerId) throws MessagingException{
		setSubject(freeSpotMsg);
		Multipart mp = new MimeMultipart();

        MimeBodyPart htmlPart = new MimeBodyPart();
        String htmlBody = receiver.getName() + "<br>" +
        		freeSpotMsg +
        		"<br>" +
				getEventLink(groupId, eventId, eventRuleName) +
		        "<br><br>" +
		        "Will you be participating in this event?" +
		        "<br>" +
		        getAnswerLink(true, groupId, eventId, answerId)+
		        "<br>" +
		        getAnswerLink(false, groupId, eventId, answerId);
        htmlPart.setContent(htmlBody, "text/html");		
        mp.addBodyPart(htmlPart);
        setMultiPartMessage(mp);
        send();
	}
	
	public void sendLockedListMsg(Event e, String groupName, String eventRuleName, ArrayList<String> partList) throws MessagingException{
		setSubject(groupName + ", " + e.getEventRule().getName());
		Multipart mp = new MimeMultipart();

		StringBuilder sb = new StringBuilder();
		for(String a : partList)
			sb.append("<br>" + a);
        MimeBodyPart htmlPart = new MimeBodyPart();
        String htmlBody = groupName + ", " + e.getEventRule().getName() +
				getEventLink(e.getGroupId(), e.getId(), e.getEventRule().getName()) +
				sb.toString();;
        htmlPart.setContent(htmlBody, "text/html");		
        mp.addBodyPart(htmlPart);
        setMultiPartMessage(mp);
        send();
	}
	
	private String getEventLink(long groupId, long eventId, String eventRuleName){
		return "<a href=\"http://1-dot-front-shuttle.appspot.com/external/gotoEvent.jsp" +
				"?groupId=" + String.valueOf(groupId) + 
		        "&eventId=" + String.valueOf(eventId) + 
		        "&token=" + receiver.getToken() + 
		        "\">" + eventRuleName.toUpperCase() + "</a>";
	}
	
	private String getAnswerLink(boolean willParticipate, long groupId, long eventId, long answerId){
		return "<a href=\"http://1-dot-front-shuttle.appspot.com/external/gotoAnswer.jsp" +
				"?groupId=" + String.valueOf(groupId) + 
		        "&eventId=" + String.valueOf(eventId) +  
		        "&answerId=" + String.valueOf(answerId) +
		        "&willParticipate=" + String.valueOf(willParticipate) +
		        "&token=" + receiver.getToken() + 
		        "\">" + (willParticipate ? "YES" : "NO") + "</a>";
	}
	
	public Message getMessage(){
		return msg;
	}
	
	private void send() throws MessagingException{
		Transport.send(msg);
	}
}
