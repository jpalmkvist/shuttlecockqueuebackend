package se.queue.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.mail.MessagingException;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import se.exception.GcmError;
import se.queue.model.Event;
import se.queue.model.User;

public class GcmHelper implements ContactMethod{
	private static final String API_KEY = "AIzaSyDwO-faZhMVciO164x3gn_dEJH_Xlwj4_4";
	private static final String ACTION = "action",
            ACT_NEW_EVENT = "new_event",
            ACT_REMINDER = "reminder",
            ACT_LIST_LOCKED = "list_locked",
            ACT_FREE_SPOT = "free_spot"; 
	private Sender sender;
	private User receiver;
	protected Logger logger;
	private String subject;
	
	public GcmHelper(User receiver, String subject){
		sender = new Sender(API_KEY);
		this.receiver = receiver;
		logger = Logger.getLogger("mLogger");
		this.subject = subject;
	}
	
	private Message createMessage(String action, long groupId, long eventId, String msg){
		Message message = new Message.Builder()
		.addData(ACTION, action)
		.addData("subject", subject)
		.addData("group_id", String.valueOf(groupId))
		.addData("event_id", String.valueOf(eventId))
		.addData("msg", msg)
		.build();
		return message;
	}

	public void sendCreationMsg(String eventRuleName, String creationMsg,
			long groupId, long eventId, long answerId)
			throws MessagingException, GcmError {
		Message message = createMessage(ACT_NEW_EVENT, groupId, eventId, creationMsg);
		sendSingleMessage(message);
	}

	public void sendReminderMsg(String reminderMsg, String eventRuleName,
			long groupId, long eventId, long answerId)
			throws MessagingException, GcmError {
		Message message = createMessage(ACT_REMINDER, groupId, eventId, reminderMsg);
		sendSingleMessage(message);
		
	}

	public void sendLockedListMsg(Event e, String groupName,
			String eventRuleName, ArrayList<String> partList)
			throws MessagingException, GcmError {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < partList.size(); i++)
			sb.append(partList.get(i) + " ");
		Message message = createMessage(ACT_LIST_LOCKED, e.getGroupId(), e.getId(), sb.toString());
		sendSingleMessage(message);
		
	}

	public void sendFreeSpotMsg(String eventRuleName, String freeSpotMsg,
			long groupId, long eventId, long answerId)
			throws MessagingException, GcmError {
		Message message = createMessage(ACT_FREE_SPOT, groupId, eventId, freeSpotMsg);
		sendSingleMessage(message);
		
	}
	
	private void sendSingleMessage(Message message) throws GcmError {
		logger.warning("Sending message to device " + receiver.getAppId());
		Result result;
		try {
			result = sender.sendNoRetry(message, receiver.getAppId());
		} catch (IOException e) {
			throw new GcmError("ioException");
			//logger.log(Level.SEVERE, "Exception posting " + message, e);
		}
		if (result == null) {
			//logger.warning("result is null, some error ");
			throw new GcmError("result is null");
		}
		if (result.getMessageId() != null) {
			logger.warning("Succesfully sent message to device " + receiver.getAppId());
			//logger.warning("messageId: " + result.getMessageId());
			/*String canonicalRegId = result.getCanonicalRegistrationId();
			if (canonicalRegId != null) {
				//logger.warning("canonical: " + canonicalRegId);
				// same device has more than on registration id: update it
			}
			else{
				logger.warning("canonical is null");
			}*/
		} else {
			String error = result.getErrorCodeName();
			if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
				//logger.warning("phone is unregistered from gcm, remove from datastore, set to mail ");
				throw new GcmError("device is not registered");
				
			} else {
				throw new GcmError("big error");
				//logger.severe("Error sending message to device " + receiver.getAppId() + ": " + error);
			}
		}
	}

}
