package se.queue.model;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Login {
	private String token;
	
	public Login(User u){
		this.token = u.getToken();
	}
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		json.put("token", token);
		return json;
	}
}
