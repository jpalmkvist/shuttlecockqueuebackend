package se.queue.servlet.user;

import java.io.IOException;

import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class SetContactMethod extends MainServlet{
	private static final long serialVersionUID = 6358003892760721114L;

	public void handleRequest() throws IOException {

		String[] token = req.getParameterValues("token");
		String[] contactMethod = req.getParameterValues("contactMethod");
		//optional
		String[] gcmId = req.getParameterValues("gcmId");

		checkParameter(token, "token");
		checkParameter(contactMethod, "contactMethod");

		if(!paramFailed){
			User user = User.loadWithToken(token[0]);
			try{
				if(user != null){
					user.setContactMethod(contactMethod[0]);
					if(user.getContactMethod() != User.CONTACT_MAIL){
						if(gcmId != null){
							user.setAppId(gcmId[0]);
						}
						else{
							user.setContactMethod("mail");
							set409Error("Unable to change the contactMethod to: " + contactMethod[0] + " if no gcmId is specified.");
						}
					}
					user.saveToDatastore();
				}
				else{
					set404Error("user was not found");
				}
			}
			catch(Exception e){
				set400Error(e.getMessage());
			}
		}
		out.close();
	}
}