package se.queue.servlet.event;

import java.io.IOException;

import se.exception.AnswerNotPartOfEventException;
import se.queue.model.Answer;
import se.queue.model.Event;
import se.queue.model.Group;
import se.queue.model.User;
import se.queue.servlet.MainServlet;

public class SetAnswer extends MainServlet{
	private static final long serialVersionUID = 2446797247089967711L;

	public void handleRequest() throws IOException {
		String[] token = req.getParameterValues("token");
		String[] groupId = req.getParameterValues("groupId");
		String[] eventId = req.getParameterValues("eventId");
		String[] answerId = req.getParameterValues("answerId");
		String[] willParticipate = req.getParameterValues("willParticipate");

		checkParameter(token, "token");
		checkParameter(groupId, "groupId");
		checkParameter(eventId, "eventId");
		checkParameter(answerId, "answerId");
		checkParameter(willParticipate, "willParticipate");

		if(!paramFailed){
			User u = User.loadWithToken(token[0]);
			long gId = Long.parseLong(groupId[0]);
			long eId = Long.parseLong(eventId[0]);
			long aId = Long.parseLong(answerId[0]);
			boolean part = Boolean.parseBoolean(willParticipate[0]);
			if(u != null){
				Group g = Group.loadFromId(gId);
				if(g != null){
					Event e = Event.loadFromId(eId);
					if(e != null){
						Answer a = Answer.loadFromId(aId);
						if(a != null){
							if(a.getUserId() == u.getId()){
								if(!e.isListLocked()){
									try {
										e.setAnswer(a, part);
									} catch (AnswerNotPartOfEventException e1) {
										set409Error(e1.getMessage());
									}
								}
								else
									set409Error("This event is locked, you can not change your answer");
							}
							else
								set403Error("This user can not use this answer");
						}
						else
							set404Error("answer was not found");
					}
					else
						set404Error("event was not found");
				}
				else
					set404Error("group was not found");
			}
			else
				set404Error("user was not found");
		}
		out.close();
	}



}
